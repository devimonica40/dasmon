<?php
include "../session.php";
require_once '../class/class.select.php';
$select = new select;
if(isset($_GET['kode'])){
    $kode = $_GET['kode'];
}else{
    $kode = null;
}

if(isset($_GET['id'])){
    $id_log = $_GET['id'];
}else{
    $id_log = null;
}

if(isset($_GET['status'])){
    $status = $_GET['status'];
}else{
    $status = null;
}

$result = $select->get_detail_mesin($kode);
$row    = mysql_fetch_array($result);


// include '../class/koneksi.php';
//     $kode = $_POST['kode'];
//     $sql= mysql_query("SELECT * FROM m_mesin WHERE kode_mesin = '$kode' ");
//     $result = mysql_fetch_array($sql);
//     echo json_encode($result);
echo '
           <div class="row clearfix">
               <div class="col-12">
                  <div class="panel-body content-body">
                    <div class="form-group row">
                    <input type="hidden" id="id_log" value="'.$id_log.'">
                    <input type="hidden" id="kode_mesin_updt" value="'.$row['kode_mesin'].'">
					<input type="hidden" id="id_notif" value="'.$status.'">
					<input type="hidden" id="id_teknisi" value="'.$row['teknisi_id'].'">
                    <input type="hidden" id="realisasi_id_teknisi" value="'.$row['realisasi_teknisi_id'].'">
                    
                        <div class="col-sm-4">
                            <label class="label">Province</label>
                            <div><input class="form-control" value="'.$row['provinsi_name'].'" id="provinsi" readonly="true" oninput="suggest_prov(this.value);" ></div><div id="suggest_prov"></div>
                            </div>
                            <div class="col-sm-4">
                            <label class="label">City</label>
                            <div><input class="form-control" value="'.$row['kota_name'].'" id="kota" readonly="true" oninput="suggest(this.value);" ></div><div id="suggest"></div>
                            </div>
                            <div class="col-sm-4">
                            <label class="label">District</label>
                            <div><input class="form-control" value="'.$row['kecamatan_name'].'" id="kecamatan" readonly="true" oninput="suggest_kec(this.value);"></div><div id="suggest_kec"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4">
                            <label class="label">Village</label>
                            <div><input class="form-control" value="'.$row['desa_name'].'" id="desa"  readonly="true" oninput="suggest_desa(this.value);"></div><div id="suggest_desa"></div>
                            </div>
                            <div class="col-sm-4">
                            <label class="label">Device Name</label>
                            <div><input class="form-control" value="'.$row['trafo_name'].'" id="nama_alat" readonly="true"></div>
                            </div>
                            <div class="col-sm-4">
                            <label class="label">Device IP</label>
                            <div><input class="form-control" value="'.$row['ip'].'" id="device_ip" readonly="true"></div>
                        </div>
                    </div>
                    <br>
                    <div class="form-group row">
                        <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">Capacity </span>
                        </div>
                            <input type="number" value="'.$row['kapasitas'].'" id="capacity" class="form-control" readonly="false">
                            <div class="input-group-prepend">
                            <span class="input-group-text"><select style="margin-right: -10px; font-size:12px"><option>Amper</option><option>MVA</option></select></span>
                            </div>
                        </div>
                        <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">Serial Number </span>
                        </div>
                            <input type="text" value="'.$row['serial_number'].'" id="serial_number" class="form-control" readonly="true">
                        </div>

                        <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">CT Primer </span>
                        </div>
                            <input type="number" value="'.$row['ct_primer'].'" id="ct_primer" class="form-control" readonly oninput="hit_ratio()" onchange="hit_ratio()">
                            <div class="input-group-prepend">
                            <span class="input-group-text">A</span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group row">
                    <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">CT Sekunder </span>
                        </div>
                            <input type="number" value="'.$row['ct_sekunder'].'" id="ct_sekunder" class="form-control" readonly oninput="hit_ratio()" onchange="hit_ratio()">
                            <div class="input-group-prepend">
                            <span class="input-group-text">A</span>
                            </div>
                        </div>
                        <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">Ratio</span>
                        </div>
                            <input type="number" value="'.$row['ratio'].'" id="ratio" class="form-control" readonly>
                        </div>
                        <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">OC Setting </span>
                        </div>
                            <input type="number" value="'.$row['max_curr'].'" id="over_curr" class="form-control" readonly>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group row">
                    <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">UC Setting </span>
                        </div>
                            <input type="number" value="'.$row['min_curr'].'"  id="under_curr" class="form-control" readonly>
                        </div>
                        <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">OV Setting</span>
                        </div>
                            <input type="number" value="'.$row['max_volt'].'" id="over_volt" class="form-control" readonly>
                        </div>
                        <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">UV Setting </span>
                        </div>
                            <input type="number" value="'.$row['min_volt'].'" id="under_volt" class="form-control" readonly>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group row">
                    <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">OF Setting </span>
                        </div>
                            <input type="number" value="'.$row['freq_max'].'"  id="over_freq" class="form-control" readonly>
                        </div>
                        <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">UF Setting</span>
                        </div>
                            <input type="number" value="'.$row['freq_min'].'" id="under_freq" class="form-control" readonly>
                        </div>
                        <div class="input-group col-md-4" style="height: 20px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">Max Temp </span>
                        </div>
                            <input type="number" value="'.$row['max_temp'].'" id="max_temp" class="form-control" readonly>
                        </div>
                    </div>
                    <br>
                    <div class="form-group row">
                        <div class="col-sm-4">
                            <label class="label">Latitude</label>
                            <div><input class="form-control" value="'.$row['latitude'].'" id="latitude"  readonly="true"></div>
                            </div>
                            <div class="col-sm-4">
                            <label class="label">Longitude</label>
                            <div><input class="form-control" value="'.$row['longitude'].'" id="longitude" readonly="true"></div>
                            </div>
                            <div class="col-sm-4">
                            <label class="label">Description</label>
                            <div><input class="form-control" value="'.$row['keterangan'].'" id="keterangan" readonly="true"></div>
                            </div>
                        </div>
                  </div>
               </div>
          </div>';
?>