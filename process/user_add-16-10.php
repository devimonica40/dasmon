<?php
error_reporting(0);
include '../session.php';
include '../class/class.select.php';
$select=new select;
$userID=$_SESSION['userSession'];
$roles = $select->fetchAll('tbl_im_role');

if(!empty($_GET['kode'])){
	$kode=$_GET['kode'];
}

$qry=$select->user_edit($kode);
$row=mysql_fetch_array($qry);
?>
<form role="form" method="POST">
		<div class="row">
		<div class="col-xs-6 col-sm-6 col-md-6">
			<div class="form-group">
				<input type="hidden" value="<?php echo $row['userID'];?>" name="userID" id="userID" class="form-control input-sm" placeholder="UserID">
				<input type="text" value="<?php echo $row['nama_depan'];?>" name="txtname1" id="" class="form-control input-sm" placeholder="First Name">
			</div>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-6">
			<div class="form-group">
				<input type="text" value="<?php echo $row['nama_belakang'];?>" name="txtname2" id="" class="form-control input-sm" placeholder="Last Name">
			</div>
		</div>
	</div>
	
    <div class="form-group">
		<input type="email" value="<?php echo $row['userEmail'];?>" name="txtemail" id="" class="form-control input-sm" placeholder="Email Address">
	</div>

	<div class="form-group">
		<label for="role" class="text-muted">Role</label>
		<select class="form-control input-sm" id="role" name="cmbRole" required>
			<?php foreach($roles as $role): ?>
		<option value="<?= $role['role_rolecode']; ?>" class="role"><?= $role['role_rolecode']; ?></option>
			<?php endforeach; ?>
		</select>
	</div>

	<div class="row">
		<div class="col-xs-6 col-sm-6 col-md-6">
			<div class="form-group">
				<input type="radio" onClick="user_stat()" id="us_id_on" name="userStatus" <?php if($row['userStatus']=='Y'){echo 'checked';}?>>ACTIVE
				<input type="radio" onClick="user_stat()" id="us_id_off" name="userStatus" <?php if($row['userStatus']=='N'){echo 'checked';}?>>NONACTIVE
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 col-sm-6 col-md-6">
			<div class="form-group">
				<input type="checkbox" id="chg_pswid" onChange="chg_psw()"> Change Password
			</div>
		</div>
	</div>
	<div class="row" id="psw_fld" style="display:none">
		<div class="row">
		<div class="col-xs-6 col-sm-6 col-md-6">
			<div class="form-group">
				<input type="password" onKeyup="cek_psw()" name="txtpass" id="psw1" class="form-control input-sm ml-3" placeholder="Password">
			</div>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-6">
			<div class="form-group">
				<input type="password" style="width: 210px;" onKeyup="cek_psw()" name="txtpassconf" id="psw2" class="form-control input-sm" placeholder="Password Confirmation">
			</div>
		</div>		
	</div>
	</div>




	<b id="upd_psw" class="hide" style="display: none;">0</b>	
</form>
<script>
$('#btnAdd').attr('disabled',true);
$('#role').val('<?php echo $row['role_role_id'];?>');
chg_psw();

function chg_psw(){
	if($("#chg_pswid").is(':checked'))
	{
		$('#upd_psw').html(1);
		$('#psw_fld').css('display','block');
		$('#btnAdd').attr('disabled',true);
	}else{
		$('#upd_psw').html(0);
		$('#psw_fld').css('display','none');
		$('#btnAdd').attr('disabled',false);
	}
}
function cek_psw(){
	var psw1=$('#psw1').val();
	var psw2=$('#psw2').val();
		if(psw1==psw2){
			$('#btnAdd').attr('disabled',false);
		}else{
			$('#btnAdd').attr('disabled',true);
		}
}
</script>