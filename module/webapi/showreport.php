<?php
include 'koneksi.php';
$query = "SELECT report.kode, report.tanggal,
											(((report.ia+report.ib+report.ic)/3)*m_mesin.ratio) as current,
											((report.va+report.vb+report.vc)/3) as volt,
											((report.ta+report.tb+report.tc)/3) as temp,
											m_mesin.kode_mesin,
											m_mesin.ratio FROM report LEFT JOIN m_mesin ON report.kode = m_mesin.kode_mesin where date(report.tanggal) = date(now()) and kode_mesin = '0001' order by report.tanggal  asc
";
$result = mysqli_query($connection,$query);
$array_data = array();
while($baris = mysqli_fetch_assoc($result))
{
  $array_data[]=$baris;
}
echo json_encode($array_data);

?>
