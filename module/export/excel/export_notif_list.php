
<?php
$DB_host = "localhost";
$DB_user = "root";
$DB_pass = "";
$DB_name = "coba";

 try
 {
     $db_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
     $db_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 }
 catch(PDOException $e)
 {
     echo "ERROR : ".$e->getMessage();
 }
 


$stmt=$db_con->prepare('select id,substring(tanggal,1,10) as tanggal, substring(tanggal,11,9) as jam,lokasi, nama_alat,port,status,statuskoneksi from notif_list');
$stmt->execute();


$columnHeader ='Digital Alert System History';
$columnHeader2 = "No."."\t"."Tanggal"."\t"."Jam"."\t"."lokasi"."\t"."Nama Alat"."\t"."Channel"."\t"."Status"."\t"."koneksi";

$setData='';
$no=0;
while($rec =$stmt->FETCH(PDO::FETCH_ASSOC))
{
  $rowData = '';
  foreach($rec as $value)
  {
	$no=$no+1;
    $value = '"'. $value .'"'."\t";
    $rowData .= $value;
  }
  $setData .= trim($rowData)."\n";
}

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=DAS record sheet.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo ucwords($columnHeader)."\n".($columnHeader2)."\n".$setData."\n";

?>
