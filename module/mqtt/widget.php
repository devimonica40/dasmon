<script>
localStorage.clear();
var host ="das.pt-mgi.co.id";
var port = "8083";
var user='das';
var pass='das1234';
var topic = "das/sensor/phaseR";
var prefix="das/widget/"; 	
var datamessage;
var used;
var plotgauge =[];
var reportchart=[];
var binddata = [];
var plotpie=[];
var icon;
try{
ls_save('prefix',prefix);
}
catch (err)
{alert(err.message)}
var kapasitas = 1000;
	window.onload = function(){
	readsqlite();
	startConnect(host,port,topic);
};
function startConnect(host,port,topic,user='das',pass='das1234') {
    clientID = "clientID-" + parseInt(Math.random() * 100);
     client = new Paho.MQTT.Client(host, Number(port), clientID);
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;
	 var options = {
            useSSL: false,
            cleanSession: true,
			timeout: 3,
            onSuccess: subcribe,
			userName:user,
			password:pass
         };
    client.connect(options)
}

function subcribe(){
   //var topic= ls_read('topic');   
   var prefix= ls_read('prefix');   
   client.subscribe(prefix+'#');
}

function publish(data,topic){	
	var prefix= ls_read('prefix');
	message= new Paho.MQTT.Message(data);
	message.destinationName=prefix+topic;
    client.send(message);
}

function onConnectionLost(responseObject) {
  
	console.log(responseObject.errorMessage);
    if (responseObject.errorCode !== 0) {
       alert("koneksi mqtt terputus");
    } 
}

function onMessageArrived(message) {
	var topic=message.destinationName;
	var prefix= ls_read('prefix');
		
	 if(topic==prefix+'notif/read'){
	 	var msg=message.payloadString;
		document.getElementById("status").innerHTML=msg;
	 	if(msg=='0'){
	 		$('.nodot').removeClass('notification-dot');
	 		document.getElementById("notif-body").innerHTML='0';
	 	}else{
	 		$('.nodot').addClass('notification-dot');
	 	}
	 }
	if(topic==prefix+'notif'){
		publish('1','notif/read');	
		var temp_notif='<li>\
							<a href="javascript:void(0);">\
 							<div class="media">\
	 								<div class="media-left">\
	 									<i class="icon-info text-warning t-icon"></i>\
	 								</div>\
	 								<div class="media-body" onClick="publish(\'0\',\'notif/read\');">\
	 									<p class=\"text\" id=\"notif\"></p>\
	 									<span class=\"timestamp\" id=\"jam\"></span>\
	 								</div>\
	 							</div>\
	 						</a>\
	 					</li>';
						
	 	document.getElementById("notif-body").innerHTML=temp_notif;
		document.getElementById("notif").innerHTML=message.payloadString;
	 	document.getElementById("jam").innerHTML=getFormattedDate();
	 }
	try{
		var destination =topic.split('/')
		if(destination[3]=='phaseR' ||destination[3]=='phaseS' || destination[3]=='phaseT' || destination[3]=='tempR' ||destination[3]=='temoS' || destination[3]=='tempT' || destination[3]=='voltR' ||destination[3]=='voltS' || destination[3]=='voltT'  )
		{	
		datajson = document.getElementById("setting_gui").innerHTML
		$.each(JSON.parse(datajson), function(idx, obj) {		
			var type = obj.topic.split('/')
			if (obj.kd_widget =='1' && obj.topic ===  topic){
				plotgauge[idx].refresh(message.payloadString)
				$('#txt'+obj.design_id).html(message.payloadString+obj.unit); 
			}
			if (obj.kd_widget =='2' ){	
				datartc(message.payloadString,topic)
			}
			if (obj.kd_widget =='3'){
				datartc(message.payloadString,topic)
				updpie(obj.topic,idx,obj.unit)			
				}
			});	
			}
		}
	catch(err){
		
	}
}

	// var topic=message.destinationName;
	// var prefix= ls_read('prefix');
	// if(topic==prefix+'phaseR'){
	// 	document.getElementById("VphaseR").innerHTML=message.payloadString;
	// 	phaseR.refresh(message.payloadString);
	// }
	// if(topic==prefix+'freqR'){
	// 	document.getElementById("VfreqR").innerHTML=message.payloadString;
	// 	freqR.refresh(message.payloadString);
	// }
	// if(topic==prefix+'tempR'){
	// 	document.getElementById("VtempR").innerHTML=message.payloadString;
	// 	tempR.refresh(message.payloadString);
	// }
	// if(topic==prefix+'voltR'){
	// 	document.getElementById("VvoltR").innerHTML=message.payloadString;
	// 	voltR.refresh(message.payloadString);
	// }
	
	// if(topic==prefix+'notif'){
	// 	publish('1','notif/read');
		
	// 	var temp_notif='<li>\
	// 						<a href="javascript:void(0);">\
	// 							<div class="media">\
	// 								<div class="media-left">\
	// 									<i class="icon-info text-warning t-icon"></i>\
	// 								</div>\
	// 								<div class="media-body" onClick="publish(\'0\',\'notif/read\');">\
	// 									<p class=\"text\" id=\"notif\"></p>\
	// 									<span class=\"timestamp\" id=\"jam\"></span>\
	// 								</div>\
	// 							</div>\
	// 						</a>\
	// 					</li>';
						
	// 	document.getElementById("notif-body").innerHTML=temp_notif;
	// 	document.getElementById("notif").innerHTML=message.payloadString;
	// 	document.getElementById("jam").innerHTML=getFormattedDate();
	// }
	
	// if(topic==prefix+'notif/read'){
	// 	var msg=message.payloadString;
	// 	document.getElementById("status").innerHTML=msg;
	// 	if(msg=='0'){
	// 		$('.nodot').removeClass('notification-dot');
	// 		document.getElementById("notif-body").innerHTML='0';
	// 	}else{
	// 		$('.nodot').addClass('notification-dot');
	// 	}
	// }

		
function getFormattedDate() {
    var date = new Date();

    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;

    var str = day+'-'+month+'-'+date.getFullYear()+' '+ hour + ":" + min + ":" + sec;

    return str;
}

function startDisconnect() {
    client.disconnect();
}

function ls_save(index,data) {		
		return localStorage.setItem(index,data);
} 

function ls_read(index) {		
	return localStorage.getItem(index);
} 

function  readsqlite(){
$.get( "view.php" )
    .done(function( data ) {
				document.getElementById("setting_gui").innerHTML = data;
				$.each(JSON.parse(data), function(idx, obj) {
					var id = "design"+obj.design_id;
					var  name = obj.panel_name;	
					binddata.push([]);
					plotpie.push('');
					var parsetopic = obj.topic.split('/');
					var datatype= parsetopic[3].match(/phase/);
					
					if (parsetopic[3].match(/phase/) == 'phase'){
						icon ='icon icon-energy text-danger';
						}
					if (parsetopic[3].match(/frek/) == 'frek'){
						icon ='icon icon-energy text-danger';
						}
					else if (parsetopic[3].match(/volt/) == 'volt'){
							icon ='icon icon-energy text-primary';
						}
					else if (parsetopic[3].match(/temp/) == 'temp'){
							icon ='fa fa-thermometer-half text-success';
						}
						
					
					
					
					
					//---kode widget--------//
					/* 
					1 : realtime gauge
					2 : realtime chart
					3 : powerusage
					4 : Summary report
					5 : oil level
					6 :electricity
					*/	
					//yang dirubah //
					if (obj.kd_widget=="1"){
						plotgauge.push();
						addwggauge(obj.design_id,obj.topic,obj.panel_name,icon,obj.unit)
						
					}
					else if(obj.kd_widget=="2"){
						addwgreportchart(obj.design_id,obj.panel_name);
						//alert(binddata[i])
					} 
					else if(obj.kd_widget=="3"){
						icon ="fa fa-thermometer-half text-danger";
						addwgpie(obj.design_id,obj.topic,obj.panel_name,icon)
						//renderChart(obj.desain_id)	;
						//reportchart.push("")
					} 
					else if(obj.kd_widget=="4"){
						icon ="fa fa-thermometer-half text-danger";
						try{
						addwgmap(design_id,topic,panel_name,icon)
						}
						catch(err)
						{
							alert(err.message)
						}
						//renderChart(obj.desain_id)	;
						//reportchart.push("")
					} 
					else{
						icon ="icon icon-energy text-danger ";
					} 
				});	
			renderChart()	;
			rendergauge();
			try{
			
				}
				catch(err){
					alert(err.messages)
			}
			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(renderpie);
			
			});
}

///////////////////////////////////////////////////////nopal//////////////////////////////////////////



					//---kode widget--------//
					/* 
					1 : current
					2 : frequency
					3 : temperature
					4 : voltage
					5 : oil level
					6 :coverage area
					7 :electricity
					8 :energyusage
					9 : humidity
					*/				
			/* 		if (obj.kd_widget=="1"){
						icon ="fa fa-thermometer-half text-danger";
					}else if (obj.kd_widget=="2"){
						icon ="fa fa-thermometer-half text-danger";
					}else if(obj.kd_widget=="3"){
						icon = "fa fa-thermometer-half text-danger";
					}else if (obj.kd_widget=="4"){
						icon = "icon icon-energy text-danger";
					}else if (obj.kd_widget=="5"){
						icon = "icon icon-energy text-danger";
					}else if (obj.kd_widget=="6"){
						icon = "icon icon-energy text-danger";
					}else if (obj.kd_widget="7"){
						icon = "fa fa-bar-chart-o text-danger";
					}else if (obj.kd_widget="8"){
						icon = "fa fa-bar-chart-o text-danger";
					}else if (obj.kd_widget="9"){
						icon = "fa fa-bar-chart-o text-danger";
					} */
////////////////////////////////////////////////////////////////					


		


		
function delwidget(x) {
	$("."+x).addClass("d-none");
	$.get( "delete.php", { submit_data: 1, des_id: x } )
				.done(function( data ) {
					alert(data)		
				});
}

function widget_checked(index,data){
	ls_save(index,data)	
	$('.widget-checked').removeClass('fa fa-check-square-o text-success');
	var addwidget=ls_read(index);	
	switch(addwidget){
		case "1":return $('.widget-checked-wg1').addClass('fa fa-check-square-o text-success'); 
		case "2":return $('.widget-checked-wg2').addClass('fa fa-check-square-o text-success');
		case "3":return $('.widget-checked-wg3').addClass('fa fa-check-square-o text-success'); 
		case "temperature":return $('.widget-checked-temperature').addClass('fa fa-check-square-o text-success'); 
		case "oillevel":return $('.widget-checked-oillevel').addClass('fa fa-check-square-o text-success'); 
		case "electricity":return $('.widget-checked-electricity').addClass('fa fa-check-square-o text-success'); 
	}	
}
function readtime(){
			var today = new Date();
			var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
			var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			var dateTime = date+' '+time;
			return dateTime	
				}
function  datartc(data,kode){
		ls_save(kode,data);
	};

		
function realtimechart(){
  setInterval(function(){
	var datajson = document.getElementById("setting_gui").innerHTML
	$.each(JSON.parse(datajson), function(idx, obj) {
			var dttopic = obj.topic
			var parsetopic=obj.topic.split('/')
			binddata[idx].push({period:readtime(),CurrentR:ls_read(dttopic+'R'),CurrentS:ls_read(dttopic+'S'),CurrentT:ls_read(dttopic+'T')})		
				
			if ( obj.kd_widget =='2' ){
				reportchart[idx].setData(binddata[idx])
				var icon;
				if (parsetopic[3].match(/temp/) == 'temp'){
					icon ='fa fa-thermometer-half';
						}
				else {
					icon ='fa fa-bolt';
					}
				
				var R = parseInt(ls_read(dttopic+'R')) || 0;
				var S = parseInt(ls_read(dttopic+'S')) || 0;
				var T = parseInt(ls_read(dttopic+'T')) || 0;
				$('#txtchrt1'+obj.design_id).html(' <span class="m-r-30" ><i class="'+icon+' m-r-5 text-danger"></i>R: '+R +obj.unit+'</span>')
				$('#txtchrt2'+obj.design_id).html(' <span class="m-r-30" ><i class="'+icon+' m-r-5 text-primary"></i>S: '+S+obj.unit+'</span>')
				$('#txtchrt3'+obj.design_id).html(' <span class="m-r-30" ><i class="'+icon+' m-r-5 text-success"></i>T: '+T+obj.unit+'</span>')
				}
				if (binddata[idx].length >8){
					binddata[idx].shift(0);
				}
				//$('#txt'+obj.design_id).html(message.payloadString+obj.unit); 		
				});}, 1000);	 
}

function updpie(topic,position,unit){
		try{
				var name = topic.split('/')
				var R =  parseInt(ls_read(topic+'R')) ||0
				var S =  parseInt(ls_read(topic+'S')) ||0
				var T =  parseInt(ls_read(topic+'T')) ||0	
				var data = google.visualization.arrayToDataTable([
							['Power Usage', 'Realtime'],
						  [name[3]+ ' 	R' , R ],
						  [name[3]+ ' S', S ],
						  [name[3]+ ' T', T ],
						  ['Available',kapasitas-( R+S+T)]
				]);
				plotpie[position].draw(data, optionspie);
		}
		catch(err){
			alert(err.message)
		}
}		
</script>		

