<?php
include 'koneksi.php';

$kode=$_POST['kode'];
$filename = $_POST['name'];

// GET REPORT
$query_report = "SELECT
        report.tanggal,
        report.ia*mesin.ratio as ia,
        report.ib*mesin.ratio as ib,
        report.ic*mesin.ratio as ic,
        report.va,
        report.vb,
        report.vc,
        report.ta,
        report.tb,
        report.tc,
        report.fa,
        report.fb,
        report.fc
        FROM
        (SELECT * FROM report ) report
        INNER JOIN (SELECT * from m_mesin ) mesin ON mesin.kode_mesin = report.kode
        WHERE report.kode ='".$kode."' ORDER BY tanggal DESC LIMIT 3000";
$result_report = mysqli_query($connection,$query_report);
$array_data_report = array();
while($baris = mysqli_fetch_assoc($result_report))
{
  $array_data_report[]=$baris;
}
$data_report=json_encode($array_data_report);

$hasil_report=json_decode($data_report, true);




// CREATE XLS FILE 
$judul = "DIGITAL ALERT SYSTEM HISTORY";
$columnHeader = "No." . "\t" . "Tanggal" . "\t" . "Arus R" . "\t" . "Arus S" . "\t" . "Arus T" . "\t" . "Volt R" . "\t" . "Volt S" . "\t" . "Volt T" . "\t" . "Temp R" . "\t" . "Temp S" . "\t" . "Temp T" . "\t". "Freq R" . "\t". "Freq S" . "\t". "Freq T";
$setData = '';
$i=1;
for ($no = 0; $no <= count($hasil_report); $no++) {
    $rowData = '';
    $rowData .= '"' . $i . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['tanggal'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['ia'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['ib'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['ic'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['va'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['vb'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['vc'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['ta'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['tb'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['tc'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['fa'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['fb'] . '"' . "\t";
    $rowData .= '"' . $hasil_report[$no]['fc'] . '"' . "\t";
    $setData .= trim($rowData) . "\n";
    $i++;
} 

$data= $judul . "\n" ."\n" ."\n" .$columnHeader . "\n" . $setData . "\n";
$name = 'logs/'.$filename.'.xls';
$fp = fopen($name , 'wb');
fwrite($fp , $data );
chmod($name, 0777);
fclose($fp);
