<?php
session_start();
include '../../class/class.select.php';
include '../../class/functions.php';
$select=new select;

if(!empty($_REQUEST['kode'])){
	$kode=$_REQUEST['kode'];
	$kode_gi=$_REQUEST['kode_gi'];
}else{
	$kode='001';
	$kode_gi='001';
}
$jml_ols=$select->jml_ols($kode_gi);
?>

<html>
<head>
<title>SKEMA OLS HMI </title>
<link rel="icon" href="../../img/electrict.png" type="image/x-icon"/>
<link rel="shortcut icon" href="../../img/electrict.png" type="image/x-icon"/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="../../bower_components/fontawesome/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="../../css/AdminLTE.css">
<link rel="stylesheet" type="text/css" href="../../css/custom.css">
<link rel="stylesheet" type="text/css" href="../../css/led.css">
<link rel="stylesheet" href="../../css/magnify.css">

<style>
body {
    background-color: #000;
    background-repeat: no-repeat;
}
/*http://getspritexy.com */
.red_bullet{
background: url("images/bullets_new_small_very.png");
background-position: 0px -15px;
width: 6px;
height: 5px;
}

.green_bullet{
background: url("images/bullets_new_small_very.png");
background-position: 0px 0px;
width: 6px;
height: 5px;
}

.blue_bullet{
background: url("images/bullets_new_small_very.png");
background-position: 0px 10px;
width: 6px;
height: 5px;
}

.orange_bullet{
background: url("images/bullets_new_small_very.png");
background-position: 0px -5px;
width: 6px;
height: 5px;
}

.micom{
background: url("images/hmi_trans2.png");
background-position: -175px -70px;
width: 190px;
height: 236px;
}
.micom_hr{
background: url("images/hmi_trans2.png");
background-position: -44px -307px;
width: 200px;
height: 6px;
}

.micom2{
	z-index:10;
	position:absolute;
	left:184px;
	top:71px;
}
.micom2_hr{
	z-index:10;
	position:absolute;
	left:53px;
	top:307px;
}
.micom3{
	z-index:10;
	position:absolute;
	left:365px;
	top:71px;
}
.micom3_hr{
	z-index:10;
	position:absolute;
	left:234px;
	top:307px;
}
.micom4{
	z-index:10;
	position:absolute;
	left:555px;
	top:71px;
}
.micom4_hr{
	z-index:10;
	position:absolute;
	left:424px;
	top:307px;
}
.micom5{
	z-index:10;
	position:absolute;
	left:745px;
	top:71px;
}
.micom5_hr{
	z-index:10;
	position:absolute;
	left:614px;
	top:307px;
}

.trip{
	z-index:10;
	position:absolute;
	left:22px;
	top:133px;
}
.trip_ols1{
	z-index:10;
	position:absolute;
	left:22px;
	top:133px;
}
.alarm{
	z-index:10;
	position:absolute;
	left:22px;
	top:139px;
}
.alarm_ols1{
	z-index:10;
	position:absolute;
	left:22px;
	top:139px;
}
.warning{
	z-index:10;
	position:absolute;
	left:20px;
	top:136px;
}
.warning_ols1{
	z-index:10;
	position:absolute;
	left:20px;
	top:136px;
}
.healthy{
	z-index:10;
	position:absolute;
	left:22px;
	top:150px;
}
.healthy_ols1{
	z-index:10;
	position:absolute;
	left:22px;
	top:150px;
}
.led5{
	z-index:10;
	position:absolute;
	left:22px;
	top:156px;
}
.led5_ols1{
	z-index:10;
	position:absolute;
	left:22px;
	top:156px;
}
.led6{
	z-index:10;
	position:absolute;
	left:22px;
	top:161px;
}
.led6_ols1{
	z-index:10;
	position:absolute;
	left:22px;
	top:161px;
}

.led7{
	z-index:10;
	position:absolute;
	left:22px;
	top:167px;
}
.led7_ols1{
	z-index:10;
	position:absolute;
	left:22px;
	top:167px;
}



.led8{
	z-index:10;
	position:absolute;
	left:22px;
	top:173px;
}
.led8_ols1{
	z-index:10;
	position:absolute;
	left:22px;
	top:173px;
}

.add_btn{
	color:#FFF;
	position:absolute;
	right:50px;
	top:20px;
	cursor:pointer;
	float:left;
}
.add_btn1{
	color:#FFF;
	position:absolute;
	right:50px;
	top:50px;
	cursor:pointer;
	float:left;
}
.add_btn2{
	color:#FFF;
	position:absolute;
	right:50px;
	top:80px;
	cursor:pointer;
	float:left;
}
.add_btn3{
	color:#FFF;
	position:absolute;
	right:50px;
	top:110px;
	cursor:pointer;
	float:left;
}
.btn_led_rst{
	color:#FFF;
	position:absolute;
	left:60px;
	top:210px;
	cursor:pointer;
	float:left;
	color:#000;
}
.btn_led_blk{
	color:#FFF;
	position:absolute;
	left:60px;
	top:245px;
	cursor:pointer;
	float:left;
	color:#000;
}
.btn_led_blk0{
	color:#FFF;
	position:absolute;
	left:60px;
	top:275px;
	cursor:pointer;
	float:left;
	color:#000;
}
#lcd{
	z-index:10;
	position:absolute;
	left:27px;
	top:116px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:11px;
	width:51px;
	background-color:yellow;
	cursor:pointer;
}
#lcd .fasa_a{
	width:20px;
	height:5px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd .fasa_b{
	width:20px;
	height:5px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd .fasa_c{
	width:20px;
	height:6px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd .fasa_n{
	width:20px;
	height:6px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd_show{
	z-index:1000;
	position:absolute;
	top:60px;
	left:100px;
}

#lcd_ols1{
	z-index:10;
	position:absolute;
	left:27px;
	top:116px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:11px;
	width:51px;
	background-color:yellow;
	cursor:pointer;
    display: none;
}
#lcd_ols1 .fasa_a_ols1{
	width:20px;
	height:5px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd_ols1 .fasa_b_ols1{
	width:20px;
	height:5px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd_ols1 .fasa_c_ols1{
	width:20px;
	height:6px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd_ols1 .fasa_n_ols1{
	width:20px;
	height:6px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd_show_ols1{
	z-index:1000;
	position:absolute;
	top:60px;
	left:100px;
}

#lcd_ols2{
	z-index:10;
	position:absolute;
	left:216px;
	top:117px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:11px;
	width:51px;
	background-color:yellow;
	cursor:pointer;
	display:none;
}
#lcd_ols2 .fasa_a_ols2{
	width:20px;
	height:5px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd_ols2 .fasa_b_ols2{
	width:20px;
	height:5px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd_ols2 .fasa_c_ols2{
	width:20px;
	height:6px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd_ols2 .fasa_n_ols2{
	width:20px;
	height:6px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd_show_ols2{
	z-index:1000;
	position:absolute;
	top:60px;
	left:290px;
}



#lcd_ols3{
	z-index:10;
	position:absolute;
	left:397px;
	top:117px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:11px;
	width:51px;
	background-color:yellow;
	cursor:pointer;
	display:none;
}
#lcd_ols3 .fasa_a_ols3{
	width:20px;
	height:5px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd_ols3 .fasa_b_ols3{
	width:20px;
	height:5px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd_ols3 .fasa_c_ols3{
	width:20px;
	height:6px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd_ols3 .fasa_n_ols3{
	width:20px;
	height:6px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd_show_ols3{
	z-index:1000;
	position:absolute;
	top:60px;
	left:470px;
}


#lcd_ols4{
	z-index:10;
	position:absolute;
	left:587px;
	top:117px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:11px;
	width:51px;
	background-color:yellow;
	cursor:pointer;
	display:none;
}
#lcd_ols4 .fasa_a_ols4{
	width:20px;
	height:5px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd_ols4 .fasa_b_ols4{
	width:20px;
	height:5px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd_ols4 .fasa_c_ols4{
	width:20px;
	height:6px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd_ols4 .fasa_n_ols4{
	width:20px;
	height:6px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd_show_ols4{
	z-index:1000;
	position:absolute;
	top:60px;
	left:660px;
}


#lcd_ols5{
	z-index:10;
	position:absolute;
	left:777px;
	top:117px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:11px;
	width:51px;
	background-color:yellow;
	cursor:pointer;
	display:none;
}
#lcd_ols5 .fasa_a_ols5{
	width:20px;
	height:5px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd_ols5 .fasa_b_ols5{
	width:20px;
	height:5px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd_ols5 .fasa_c_ols5{
	width:20px;
	height:6px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:4px;
}
#lcd_ols5 .fasa_n_ols5{
	width:20px;
	height:6px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:4px;
}
#lcd_show_ols5{
	z-index:1000;
	position:absolute;
	top:60px;
	left:850px;
}


#arus{
	z-index:2;
	position:absolute;
	left:60px;
	top:210px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:90px;
	width:85px;
	background-color:black;
}
#arus_ols2{
	z-index:2;
	position:absolute;
	left:250px;
	top:210px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:100px;
	width:85px;
	background-color:black;
	display:none;
}
#arus_ols3{
	z-index:2;
	position:absolute;
	left:430px;
	top:210px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:100px;
	width:85px;
	background-color:black;
	display:none;
}
#arus_ols4{
	z-index:2;
	position:absolute;
	left:620px;
	top:210px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:100px;
	width:85px;
	background-color:black;
	display:none;
}
#arus_ols5{
	z-index:2;
	position:absolute;
	left:810px;
	top:210px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:100px;
	width:85px;
	background-color:black;
	display:none;
}

#label_ols{
	z-index:2;
	position:absolute;
	left:10px;
	top:20px;
	width:auto;
	height:auto;
	color:#fff;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:left;
	font-size:35px;
}
#nama_ols1{
	z-index:2;
	position:absolute;
	left:10px;
	top:65px;
	width:auto;
	height:20px;
	color:#fff;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:left;
	font-size:15px;	
}
#nama_ols2 {
	z-index:2;
	position:absolute;
	left:200px;
	top:65px;
	width:auto;
	height:20px;
	color:#fff;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:left;
	font-size:15px;
	display:none;
}
#nama_ols3 {
	z-index:2;
	position:absolute;
	left:380px;
	top:65px;
	width:auto;
	height:20px;
	color:#fff;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:left;
	font-size:15px;
	display:none;
}
#nama_ols4 {
	z-index:2;
	position:absolute;
	left:570px;
	top:65px;
	width:auto;
	height:20px;
	color:#fff;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:left;
	font-size:15px;
	display:none;
}
#nama_ols5 {
	z-index:2;
	position:absolute;
	left:760px;
	top:65px;
	width:auto;
	height:20px;
	color:#fff;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:left;
	font-size:15px;
	display:none;
}
.arus01{
	width:70px;
	height:20px;
	color:#fff;
	float:right;
	border:solid 0px red;
	margin:0px;
	margin-top:0px;
	text-align:right;
	font-size:11px;
}

.label{
	width:10px;
	height:20px;
	color:#fff;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	text-align:left;
	font-size:12px;
}

#warna_target{
	z-index:11;
	position:absolute;
	left:204px;
	top:516px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	padding:1px;
	border:solid 0px white;
	height:72px;
	width:80px;
	/*background-color:green;*/
	padding:5px;
}
#warna_target2{
	z-index:11;
	position:absolute;
	left:446px;
	top:516px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	padding:1px;
	border:solid 0px white;
	height:72px;
	width:80px;
	/*background-color:green;*/
	padding:5px;
}
#warna_target3{
	z-index:11;
	position:absolute;
	left:687px;
	top:516px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	padding:1px;
	border:solid 0px white;
	height:72px;
	width:80px;
	/*background-color:green;*/
	padding:5px;
}
#warna_target4{
	z-index:11;
	position:absolute;
	left:928px;
	top:516px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	padding:1px;
	border:solid 0px white;
	height:72px;
	width:80px;
	/*background-color:green;*/
	padding:5px;
}
.wr_target{
	background-color:white;
}
.red_bg{
	background-color:red;
}
.green_bg{
	background-color:green;
}
.yellow_bg{
	background-color:yellow;
}
#label_arus_total{
	top:320px;
	left:60px;
	width:125px;
	height:20px;
	color:#fff;
	float:left;
	margin:0px;
	margin-top:0px;
	margin-right:130px;
	text-align:left;
	font-size:12px;
	border:solid 0px red;
	position:absolute;
}
.arus_total{	
	font-size:12px;
}
#arus_target{
	z-index:11;
	position:absolute;
	left:300px;
	top:518px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	height:68px;
	width:85px;
	background-color:black;
}
#label_trafo{
	z-index:12;
	position:absolute;
	left:168px;
	top:380px;
	color:#666;
	font-family:arial;
	font-size:11px;
	font-weight:bold;
	border:solid 0px red;
	height:20px;
	width:auto;
	background-color:black;
}
.label_isi{
	height:20px;
	color:#fff;
	float:left;
	margin:0px;
	margin-top:0px;
	text-align:left;
	font-size:12px;
	border:solid 1px red;
}
.trafo1{
	width:125px;
	height:20px;
	color:#fff;
	float:left;
	margin:0px;
	margin-top:0px;
	margin-right:130px;
	text-align:left;
	font-size:12px;
	border:solid 0px red;
}
.trafo2{
	width:125px;
	height:20px;
	color:#fff;
	float:left;
	margin:0px;
	margin-top:0px;
	margin-right:130px;
	text-align:left;
	font-size:12px;
	border:solid 0px red;
}
.trafo3{
	width:125px;
	height:20px;
	color:#fff;
	float:left;
	margin:0px;
	margin-top:0px;
	margin-right:130px;
	text-align:left;
	font-size:12px;
	border:solid 0px red;
}
.trafo4{
	width:125px;
	height:20px;
	color:#fff;
	float:left;
	margin:0px;
	margin-top:0px;
	text-align:left;
	font-size:12px;
	border:solid 0px red;
}

.target2{	
	position:absolute;
	left:297px;
	top:402px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	background-color:black;
	z-index:2;
	background: url("images/hmi_trans2.png");
	background-position: -46px -403px; 
	width: 243px;
	height: 189px;
}
.target3{	
	position:absolute;
	left:538px;
	top:402px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	background-color:black;
	z-index:2;
	background: url("images/hmi_trans2.png");
	background-position: -46px -403px; 
	width: 243px;
	height: 189px;
}
.target4{	
	position:absolute;
	left:779px;
	top:402px;
	color:#666;
	font-family:arial;
	font-size:8px;
	font-weight:bold;
	border:solid 0px red;
	background-color:black;
	z-index:2;
	background: url("images/hmi_trans2.png");
	background-position: -46px -403px; 
	width: 243px;
	height: 189px;
}

.fasa_a_det{
	width:60px;
	height:5px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:0px;
	margin-top:0px;
	font-size:18px;
}

.green_bullet_det{
	background: url("images/bullets_new_small.png");
	background-position: 0px 0px;
	width: 10px;
	height: 10px;
}
.orange_bullet_det{
	background: url("images/bullets_new_small.png");
	background-position: 0px -9px;
	width: 10px;
	height: 10px;
}
.blue_bullet_det{
	background: url("images/bullets_new_small.png");
	background-position: 0px 18px;
	width: 10px;
	height: 10px;
}
.red_bullet_det{
	background: url("images/bullets_new_small.png");
	background-position: 0px -26px;
	width: 10px;
	height: 10px;
}		

<!--#097479-->
</style>
<script type="text/javascript">

	function addCurr(num) {
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/10).toString();
		if(cents<10)
		cents = "" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+'.'+
		num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + num );
	}

	function rmCurr(nil){
	  var nil=nil.replace('.','');
	  return nil;
	}
	function zf( number, width )
	{
	  width -= number.toString().length;
	  if ( width > 0 )
	  {
		return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
	  }
	  return number;
	}
	function scan_arus(){ 
	var kdgi=zf('<?php echo $kode_gi;?>');
		$.ajax({method: "GET",url:'arus_scanv2.php?kodegi='+kdgi})
		//$.ajax({method: "GET",url:'arus_scanv2.php?kodegi=003'})
				.done(function( msg_arus )
				 {
					var parsing=msg_arus.split('*');
				 	for (i=0 ; i<(parsing.length) ;i++){						
						var dt=parsing[i].split('#');							
						if(i==0){
							var ratiox=dt[8];
							if(ratiox==2000){
								$('.fasa_a_ols1').html(addCurr(dt[2]/100)+' kA');
								$('.fasa_b_ols1').html(addCurr(dt[3]/100)+' kA');
								$('.fasa_c_ols1').html(addCurr(dt[4]/100)+' kA');
								
								$('.fasa_a_ols_det1').html(addCurr(dt[2]/100)+' kA');
								$('.fasa_b_ols_det1').html(addCurr(dt[3]/100)+' kA');
								$('.fasa_c_ols_det1').html(addCurr(dt[4]/100)+' kA');
							}else{
								$('.fasa_a_ols1').html(addCurr(dt[2])+' A');
								$('.fasa_b_ols1').html(addCurr(dt[3])+' A');
								$('.fasa_c_ols1').html(addCurr(dt[4])+' A');
								
								$('.fasa_a_ols_det1').html(addCurr(dt[2])+' A');
								$('.fasa_b_ols_det1').html(addCurr(dt[3])+' A');
								$('.fasa_c_ols_det1').html(addCurr(dt[4])+' A');
							}
							
						}else if(i==1){
							if(ratiox==2000){
								$('.fasa_a_ols2').html(addCurr(dt[2]/100)+' kA');
								$('.fasa_b_ols2').html(addCurr(dt[3]/100)+' kA');
								$('.fasa_c_ols2').html(addCurr(dt[4]/100)+' kA');
								
								$('.fasa_a_ols_det2').html(addCurr(dt[2]/100)+' kA');
								$('.fasa_b_ols_det2').html(addCurr(dt[3]/100)+' kA');
								$('.fasa_c_ols_det2').html(addCurr(dt[4]/100)+' kA');
							}else{
								$('.fasa_a_ols2').html(addCurr(dt[2])+' A');
								$('.fasa_b_ols2').html(addCurr(dt[3])+' A');
								$('.fasa_c_ols2').html(addCurr(dt[4])+' A');
								
								$('.fasa_a_ols_det2').html(addCurr(dt[2])+' A');
								$('.fasa_b_ols_det2').html(addCurr(dt[3])+' A');
								$('.fasa_c_ols_det2').html(addCurr(dt[4])+' A');
							}
						}else if(i==2){
							if(ratiox==2000){
								$('.fasa_a_ols3').html(addCurr(dt[2]/100)+' kA');
								$('.fasa_b_ols3').html(addCurr(dt[3]/100)+' kA');
								$('.fasa_c_ols3').html(addCurr(dt[4]/100)+' kA');
								
								$('.fasa_a_ols_det3').html(addCurr(dt[2]/100)+' kA');
								$('.fasa_b_ols_det3').html(addCurr(dt[3]/100)+' kA');
								$('.fasa_c_ols_det3').html(addCurr(dt[4]/100)+' kA');
							}else{
								$('.fasa_a_ols3').html(addCurr(dt[2])+' A');
								$('.fasa_b_ols3').html(addCurr(dt[3])+' A');
								$('.fasa_c_ols3').html(addCurr(dt[4])+' A');
								
								$('.fasa_a_ols_det3').html(addCurr(dt[2])+' A');
								$('.fasa_b_ols_det3').html(addCurr(dt[3])+' A');
								$('.fasa_c_ols_det3').html(addCurr(dt[4])+' A');
							}
						}else if(i==3){
							if(ratiox==2000){
								$('.fasa_a_ols4').html(addCurr(dt[2]/100)+' kA');
								$('.fasa_b_ols4').html(addCurr(dt[3]/100)+' kA');
								$('.fasa_c_ols4').html(addCurr(dt[4]/100)+' kA');
								
								$('.fasa_a_ols_det4').html(addCurr(dt[2]/100)+' kA');
								$('.fasa_b_ols_det4').html(addCurr(dt[3]/100)+' kA');
								$('.fasa_c_ols_det4').html(addCurr(dt[4]/100)+' kA');
							}else{
								$('.fasa_a_ols4').html(addCurr(dt[2])+' A');
								$('.fasa_b_ols4').html(addCurr(dt[3])+' A');
								$('.fasa_c_ols4').html(addCurr(dt[4])+' A');
								
								$('.fasa_a_ols_det4').html(addCurr(dt[2])+' A');
								$('.fasa_b_ols_det4').html(addCurr(dt[3])+' A');
								$('.fasa_c_ols_det4').html(addCurr(dt[4])+' A');
							}
						}else if(i==4){
							if(ratiox==2000){
								$('.fasa_a_ols5').html(addCurr(dt[2]/100)+' kA');
								$('.fasa_b_ols5').html(addCurr(dt[3]/100)+' kA');
								$('.fasa_c_ols5').html(addCurr(dt[4]/100)+' kA');
								
								$('.fasa_a_ols_det5').html(addCurr(dt[2]/100)+' kA');
								$('.fasa_b_ols_det5').html(addCurr(dt[3]/100)+' kA');
								$('.fasa_c_ols_det5').html(addCurr(dt[4]/100)+' kA');
							}else{
								$('.fasa_a_ols5').html(addCurr(dt[2])+' A');
								$('.fasa_b_ols5').html(addCurr(dt[3])+' A');
								$('.fasa_c_ols5').html(addCurr(dt[4])+' A');
								
								$('.fasa_a_ols_det5').html(addCurr(dt[2])+' A');
								$('.fasa_b_ols_det5').html(addCurr(dt[3])+' A');
								$('.fasa_c_ols_det5').html(addCurr(dt[4])+' A');
							}
						}						
					}
										
					
					/* $('.fasa_a').html(addCurr(parsing[0])+' A');
					$('.fasa_b').html(addCurr(dt[1])+' A');
					$('.fasa_c').html(addCurr(dt[2])+' A');
					$('.fasa_n').html(addCurr(dt[3])+' A');
					$('.freq').html((dt[4])+' Hz');
					$('.volt').html((dt[5])+' KV');
					$('.arus_sum').html(addCurr((dt[0]*1+dt[1]*1+dt[2]*1)/3)+' A'); */					
					
				 });
				 
				 $.ajax({method: "GET",url:'tot_arus.php'})
				 .done(function( tot_arus )
				 {	
					var niltot=$('#niltot').html(tot_arus);
					$('.arus_total').html(addCurr(tot_arus));
					var nil_arus=tot_arus/4;
					$('.fasa_a_tar1').html(nil_arus +' A');
					$('.fasa_a_tar2').html(nil_arus +' A');
					$('.fasa_a_tar3').html(nil_arus +' A');
					$('.fasa_a_tar4').html(nil_arus +' A');
				 });
	}

	function incCounter() {
		setTimeout('incCounter()',1000);
		var nil=parseInt($('#nil').html());
		var count=nil+1;
		var dsp=$('#nil').html(count);
		if(count==2){
		  $('#nil').html(0);
		}

		$.ajax({method: "GET",url:'cektimer_tar.php?x='+<?php echo $kode;?>})
		 .done(function( msg )
		 {//alert(<?php echo $kode;?>)
		 var dt=msg.split('#');
			$('#t1').html(dt[0]);
			$('#t2').html(dt[1]);
			$('#t3').html(dt[2]);
			$('#t4').html(dt[3]);
		}
		)

            $.ajax({method: "GET", url: 'status_ols_all.php?gi=<?php echo $kode_gi;?>'})
            .done(function (msg_arus) {
                var parsing = msg_arus.split('*');
                var jml = 1;
                for (ix = 0; ix < ((parsing.length - 1)); ix++) {
                    var dt = parsing[ix].split('#');
                    var msg0=(dt[0]);
                    var msg=(dt[1]);
					var dc =(dt[2]);
					//alert(dc);
                    //alert(msg0+'='+msg)
                    /*
                    0  	:healthy			(register 12 / 000C)
                    10 	:alarm				(register 12 / 000C)
                    8  	:healthy			(register 12 / 000C)
                    2	:trip				(register 18 / 0012)
                    11 	:trip dan alarm		(register 12 / 000C)
                    12 	:blocking			(register 12 / 000C)

                    26	:alarm tahap1 blocking
                    27	:alarm tahap1 non blocking
                    58	:alarm tahap2 blocking
                    59	:alarm tahap2 non blocking
                    122	:alarm tahap3 blocking
                    123	:alarm tahap3 non blocking
                    250	:alarm tahap4 blocking
                    251	:alarm tahap4 non blocking
                    */

                    /*
                    reg:26
                        *alarm tahap1 blocking
                        *healthy
                        *led 5
                    reg:27
                        *trip
                        *alarm tahap1 non blocking
                        *healthy
                        *led 5

                    reg:58
                        *alarm tahap2 blocking
                        *healthy
                        *led 5
                        *led 6
                    reg:59
                        *trip
                        *alarm tahap2 non blocking
                        *healthy
                        *led 5
                        *led 6

                    reg:122
                        *alarm tahap3 blocking
                        *healthy
                        *led 5
                        *led 6
                        *led 7
                    reg:123
                        *trip
                        *alarm tahap3 non blocking
                        *healthy
                        *led 5
                        *led 6
                        *led 7

                    reg:250
                        *alarm tahap4 blocking
                        *healthy
                        *led 5
                        *led 6
                        *led 7
                    reg:251
                        *trip
                        *alarm tahap4 non blocking
                        *healthy
                        *led 5
                        *led 6
                        *led 7
                    */
					
				if(dc=='1'){
					$('#lcd_ols'+jml).show();
					if (msg == 0 || msg == 2 || msg == 8 || msg == 10 || msg == 11 || msg == 12 || msg == 26 || msg == 27 || msg == 58 || msg == 59 || msg == 122 || msg == 123 || msg == 250 || msg == 251) {
                       // $('.healthy').addClass('green_bullet');
                        $('.lcd_color').css('background-color', 'yellow');
                        //for (i = 1; i <= 5; i++) {
                            $('.healthy_ols' + jml).addClass('green_bullet');
                            $('.healthy_ols_det' + jml).addClass('green_bullet_det');
                        //}
                    }
                   	if (msg == '8') {                       
							$('.trip_ols'+jml).removeClass('red_bullet');
							$('.alarm_ols'+jml).removeClass('orange_bullet');
							$('.led5_ols'+jml).removeClass('red_bullet');
							$('.led6_ols'+jml).removeClass('red_bullet');
							$('.led7_ols'+jml).removeClass('red_bullet');
							$('.id_alarm'+jml).hide();
							$('.id_fasa'+jml).show();
							scan_arus();
							
							//Detail micom//
							$('.trip_ols_det'+jml).removeClass('red_bullet_det');
							$('.alarm_ols_det'+jml).removeClass('orange_bullet_det');
							$('.warning_ols_det'+jml).removeClass('red_bullet_det');
							$('.led5_ols_det'+jml).removeClass('red_bullet_det');
							$('.led6_ols_det'+jml).removeClass('red_bullet_det');
							$('.led7_ols_det'+jml).removeClass('red_bullet_det');							
						}
						
						else if(msg=='10'){
							$('.trip_ols'+jml).removeClass('red_bullet');
							$('.alarm_ols'+jml).addClass('orange_bullet');
							$('.led5_ols'+jml).removeClass('red_bullet');
							$('.led6_ols'+jml).removeClass('red_bullet');
							$('.led7_ols'+jml).removeClass('red_bullet');
							$('.id_alarm'+jml).show();
							$('.id_fasa'+jml).hide();
							add_blink();
							scan_arus();
							//Detail micom//
							$('.trip_ols_det'+jml).removeClass('red_bullet_det');
							$('.alarm_ols_det'+jml).addClass('orange_bullet_det');
							$('.warning_ols_det'+jml).removeClass('red_bullet_det');
							$('.led5_ols_det'+jml).removeClass('red_bullet_det');
							$('.led6_ols_det'+jml).removeClass('red_bullet_det');
							$('.led7_ols_det'+jml).removeClass('red_bullet_det');
						} 	
						else if(msg=='11'){
							$('.trip_ols'+jml).addClass('red_bullet');
							$('.alarm_ols'+jml).addClass('orange_bullet');
							$('.led5_ols'+jml).removeClass('red_bullet');
							$('.led6_ols'+jml).removeClass('red_bullet');
							$('.led7_ols'+jml).removeClass('red_bullet');
							$('.id_alarm'+jml).show();
							$('.id_fasa'+jml).hide();
							var cek_timer_t1=$('#t1').html()*1;
							add_blink();
							
							//Detail micom//
							$('.trip_ols_det'+jml).addClass('red_bullet_det');
							$('.alarm_ols_det'+jml).addClass('orange_bullet_det');
							$('.warning_ols_det'+jml).removeClass('red_bullet_det');
							$('.led5_ols_det'+jml).removeClass('red_bullet_det');
							$('.led6_ols_det'+jml).removeClass('red_bullet_det');
							$('.led7_ols_det'+jml).removeClass('red_bullet_det');
						}
						else if(msg=='26'){
							$('.trip_ols'+jml).removeClass('red_bullet');
							$('.alarm_ols'+jml).addClass('orange_bullet');
							$('.id_alarm'+jml).show();
							$('.id_fasa'+jml).hide();
							$('.led5_ols'+jml).addClass('red_bullet');
							$('.led6_ols'+jml).removeClass('red_bullet');
							$('.led7_ols'+jml).removeClass('red_bullet');	
							add_blink()
							
							//Detail micom//
							$('.trip_ols_det'+jml).removeClass('red_bullet_det');
							$('.alarm_ols_det'+jml).addClass('orange_bullet_det');
							$('.warning_ols_det'+jml).removeClass('red_bullet_det');
							$('.led5_ols_det'+jml).addClass('red_bullet_det');
							$('.led6_ols_det'+jml).removeClass('red_bullet_det');
							$('.led7_ols_det'+jml).removeClass('red_bullet_det');
						}
						else if(msg=='27'){
							$('.trip_ols'+jml).addClass('red_bullet');
							$('.alarm_ols'+jml).addClass('orange_bullet');
							$('.id_alarm'+jml).show();
							$('.id_fasa'+jml).hide();
							$('.led5_ols'+jml).addClass('red_bullet');
							$('.led6_ols'+jml).removeClass('red_bullet');
							$('.led7_ols'+jml).removeClass('red_bullet');
							add_blink()
							
							//Detail micom//
							$('.trip_ols_det'+jml).addClass('red_bullet_det');
							$('.alarm_ols_det'+jml).addClass('orange_bullet_det');
							$('.warning_ols_det'+jml).removeClass('red_bullet_det');
							$('.led5_ols_det'+jml).addClass('red_bullet_det');
							$('.led6_ols_det'+jml).removeClass('red_bullet_det');
							$('.led7_ols_det'+jml).removeClass('red_bullet_det');
						}
						else if(msg=='58'){
							$('.trip_ols'+jml).removeClass('red_bullet');
							$('.alarm_ols'+jml).addClass('orange_bullet');
							$('.id_alarm'+jml).show();
							$('.id_fasa'+jml).hide();
							$('.led5_ols'+jml).addClass('red_bullet');
							$('.led6_ols'+jml).addClass('red_bullet');
							$('.led7_ols'+jml).removeClass('red_bullet');
							add_blink()
							
							//Detail micom//
							$('.trip_ols_det'+jml).removeClass('red_bullet_det');
							$('.alarm_ols_det'+jml).addClass('orange_bullet_det');
							$('.warning_ols_det'+jml).removeClass('red_bullet_det');
							$('.led5_ols_det'+jml).addClass('red_bullet_det');
							$('.led6_ols_det'+jml).addClass('red_bullet_det');
							$('.led7_ols_det'+jml).removeClass('red_bullet_det');
						}
						else if(msg=='59'){
							$('.trip_ols'+jml).addClass('red_bullet');
							$('.alarm_ols'+jml).addClass('orange_bullet');
							$('.id_alarm'+jml).show();
							$('.id_fasa'+jml).hide();
							$('.led5_ols'+jml).addClass('red_bullet');
							$('.led6_ols'+jml).addClass('red_bullet');
							$('.led7_ols'+jml).removeClass('red_bullet');
							add_blink()
							
							//Detail micom//
							$('.trip_ols_det'+jml).addClass('red_bullet_det');
							$('.alarm_ols_det'+jml).addClass('orange_bullet_det');
							$('.warning_ols_det'+jml).removeClass('red_bullet_det');
							$('.led5_ols_det'+jml).addClass('red_bullet_det');
							$('.led6_ols_det'+jml).addClass('red_bullet_det');
							$('.led7_ols_det'+jml).removeClass('red_bullet_det');
						}
						else if(msg=='122'){
							$('.trip_ols'+jml).removeClass('red_bullet');
							$('.alarm_ols'+jml).addClass('orange_bullet');
							$('.id_alarm'+jml).show();
							$('.id_fasa'+jml).hide();
							$('.led5_ols'+jml).addClass('red_bullet');
							$('.led6_ols'+jml).addClass('red_bullet');
							$('.led7_ols'+jml).addClass('red_bullet');
							add_blink()
							
							//Detail micom//
							$('.trip_ols_det'+jml).removeClass('red_bullet_det');
							$('.alarm_ols_det'+jml).addClass('orange_bullet_det');
							$('.warning_ols_det'+jml).removeClass('red_bullet_det');
							$('.led5_ols_det'+jml).addClass('red_bullet_det');
							$('.led6_ols_det'+jml).addClass('red_bullet_det');
							$('.led7_ols_det'+jml).addClass('red_bullet_det');
						}
												
						else if(msg == '123') {
							$('.trip_ols'+jml).addClass('red_bullet');
							$('.trip_ols'+jml).addClass('red_bullet');
							$('.alarm_ols'+jml).addClass('orange_bullet');
							$('.id_alarm'+jml).show();
							$('.id_fasa'+jml).hide();
							$('.led5_ols'+jml).addClass('red_bullet');
							$('.led6_ols'+jml).addClass('red_bullet');
							$('.led7_ols'+jml).addClass('red_bullet');
							add_blink()
							//scan_arus();
							
							//Detail micom//
							$('.trip_ols_det'+jml).addClass('red_bullet_det');
							$('.alarm_ols_det'+jml).addClass('orange_bullet_det');
							$('.warning_ols_det'+jml).removeClass('red_bullet_det');
							$('.led5_ols_det'+jml).addClass('red_bullet_det');
							$('.led6_ols_det'+jml).addClass('red_bullet_det');
							$('.led7_ols_det'+jml).addClass('red_bullet_det');
                    }
                    var cek_timer_t1=$('#t1').html()*1;
					//alert()
						if(cek_timer_t1==1){
							//alert(1)
							$('#warna_target').removeClass('yellow_bg'); //Remove red bg
							$('#warna_target').removeClass('green_bg'); //Circuit breaker close
							$('#warna_target').addClass('red_bg'); //Circuit breaker open
						}else{
							$('#warna_target').removeClass('red_bg'); //Remove red bg
							$('#warna_target').removeClass('yellow_bg'); //Circuit breaker close
							$('#warna_target').addClass('green_bg'); //Circuit breaker open
							//alert(0)
						}

					var cek_timer_t2=$('#t2').html()*1;
						if(cek_timer_t2==1){ //alert(cek_timer_t2)
							$('#warna_target2').removeClass('yellow_bg'); //Remove red bg
							$('#warna_target2').removeClass('green_bg'); //Circuit breaker close
							$('#warna_target2').addClass('red_bg'); //Circuit breaker open
						}else{
							$('#warna_target2').removeClass('red_bg'); //Remove red bg
							$('#warna_target2').removeClass('yellow_bg'); //Circuit breaker close
							$('#warna_target2').addClass('green_bg'); //Circuit breaker open
						}

					var cek_timer_t3=$('#t3').html()*1;
						if(cek_timer_t3==1){
							$('#warna_target3').removeClass('yellow_bg'); //remove red bg
							$('#warna_target3').removeClass('green_bg'); //Circuit breaker close
							$('#warna_target3').addClass('red_bg'); //Circuit breaker open
						}else{
							$('#warna_target3').removeClass('red_bg'); //Remove red bg
							$('#warna_target3').removeClass('yellow_bg'); //Circuit breaker close
							$('#warna_target3').addClass('green_bg'); //Circuit breaker open
						}

					var cek_timer_t4=$('#t4').html()*1;
						if(cek_timer_t4==1){
							$('#warna_target4').removeClass('yellow_bg'); //remove red bg
							$('#warna_target4').removeClass('green_bg'); //Circuit breaker close
							$('#warna_target4').addClass('red_bg'); //Circuit breaker open
						}else{
							$('#warna_target4').removeClass('red_bg'); //Remove red bg
							$('#warna_target4').removeClass('yellow_bg'); //Circuit breaker close
							$('#warna_target4').addClass('green_bg'); //Circuit breaker open
						}
			
					
					    		}
				else
					{		$('.healthy').removeClass('green_bullet'); 
							$('#lcd_ols'+jml).hide();
							$('.healthy_ols'+ jml).removeClass('green_bullet');
							$('.trip_ols'+jml).removeClass('red_bullet');
							$('.alarm_ols'+jml).removeClass('orange_bullet');
							$('.led5_ols'+jml).removeClass('red_bullet');
							$('.led6_ols'+jml).removeClass('red_bullet');
							$('.led7_ols'+jml).removeClass('red_bullet');
					}	
                jml++
                }
            })

    //end
	}
	

	function add_blink(){
		for(i=0;i<=2;i++) {
	     $('.alarm').effect("pulsate", {}, 350);
		 $('.alarm_ols1').effect("pulsate", {}, 350);
		 $('.alarm_ols_det1').effect("pulsate", {}, 350);
		 add_blink_ols();		 
		 }
	}

/* 	function trip_sound () {
		setTimeout('trip_sound()',50);
		document.getElementById('trip_sound').play();
	}

	function trip_sound_stop () {
		//setTimeout('trip_sound()',50);
		document.getElementById('trip_sound').pause();
	}
*/
	/*function alarm_sound() {
		//setTimeout('alarm_sound()',100);
		document.getElementById('alarm_sound').play();
	}*/

    function alarm_stop() {
        var x=window.parent.document.getElementById("alarm_sound");
        var y=window.parent.document.getElementById("trip_sound");
        var z=window.parent.$(".icon-volume");
        //var x=document.getElementById('alarm_sound');
        if(x.muted==true){
            x.muted = false;
            y.muted = false;
            $('.icon-volume').removeClass('fa-volume-off');
            $('.icon-volume').addClass('fa-volume-up');
            //z.removeClass('fa-volume-off');
            //z.addClass('fa-volume-up');
        }else{
            x.muted = true;
            y.muted = true;
            $('.icon-volume').removeClass('fa-volume-up');
            $('.icon-volume').addClass('fa-volume-off');
            //z.removeClass('fa-volume-up');
            //z.addClass('fa-volume-off');
        }
    }

	function cdiv(id,id1){
		$('#'+id).append('<i id="'+id1+'"></i>');
	}

	function add_micom(x){
		cdiv('micom_id','micom_'+x);
		cdiv('micom_id','micom'+x+'_hr');
		$('#micom_'+x).addClass('micom'+x+' micom');
		$('#micom'+x+'_hr').addClass('micom'+x+'_hr micom_hr');
		$('#arus_ols'+x).css("display","block");
	}

	$( document ).ready(function() {

	});

	function detail_micom(id,kode){
		for(i=1; i<=5; i++){
		$('#ols_det'+i).hide()
		}
		$('#lcd_show_ols'+id).load('lcd_det.php?id='+id+'&kode='+kode);		
	}
	function di_input(x){
		var act=1;
		var kode='<?php echo $kode;?>';
		$.ajax({method: "GET",url:'di_input.php?act='+x+'&kode='+kode})
				 .done(function( msg )
				 {
					 //alert(msg);
				 }
				 )
	}
	function cdiv(x,y){
		$('#'+x).append('<div id="'+y+'"></div>');
	}
	
    </script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="incCounter(); add_blink();">
	<table border=1 style="border:solid 1px #ccc;">		
		<audio id="trip_sound" src="addons/train_low.mp3"></audio>
		<audio id="alarm_sound" src="addons/alarm.mp4"></audio>
		<b id="nil"style="display:none;">0</b>
		<b id="t1" style="display:none;">0</b>
		<b id="t2" style="display:none;">0</b>
		<b id="t3" style="display:none;">0</b>
		<b id="t4" style="display:none;">0</b>
		<b id="jml_ols" style="display:none;"><?php echo $jml_ols;?></b>
		<b id="niltot" style="display:none;">4000</b>
		
		<i class="trip_ols1"></i>
		<i class="alarm_ols1"></i>
		<i class="warning_ols1"></i>
		<i class="healthy_ols1"></i>
		<i class="led5_ols1"></i>
		<i class="led6"></i>
		<i class="led7"></i>
		<i class="led8"></i>
	
		
		<?php 
		for ($i=1; $i<=5; $i++){
		echo'
		<i class="trip_ols'.$i.'"></i>
		<i class="alarm_ols'.$i.'"></i>
		<i class="warning_ols'.$i.'"></i>
		<i class="healthy_ols'.$i.'"></i>		
		<i class="led5_ols'.$i.'"></i>
		<i class="led6_ols'.$i.'"></i>
		<i class="led7_ols'.$i.'"></i>
		<i class="led8_ols'.$i.'"></i>
		';
		}
		?>
		<!--<i class="add_btn" onclick="add_micom(2)">+ Add Micom 2</i>
		<i class="add_btn1" onclick="add_micom(3)">+ Add Micom 3</i>
		<i class="add_btn2" onclick="add_micom(4)">+ Add Micom 4</i>
		<i class="add_btn3" onclick="add_micom(5)">+ Add Micom 5</i>-->
		<!--<button class="btn_led_rst" onClick="di_input('reset')">LED Reset</button>
		<button class="btn_led_blk" onClick="di_input('block')">Blocking</button>
		<button class="btn_led_blk0" onClick="di_input('unblock')">Unblocking</button>-->

		<div id="label_ols">GARDU INDUK <?php echo $select->nama_gi($kode_gi);?></div>
		<div id="micom_id" class=""></div>
			
		<?php 
		$no=0;
		$qry=$select->ols_list_gi($kode_gi);
		while($row=mysql_fetch_array($qry)){
		$no=$no+1;//nopal
		echo '<div id="nama_ols'.$no.'" class="">'.$row['nama_unit'].'</div>';
		echo '  
		<div id="lcd_ols'.$no.'" class="lcd_color'.$no.'" onClick="detail_micom('.$no.',\''.$row['kode'].'\');">
				<div style="width:200p; height:102px; display:none;" class="id_alarm'.$no.'" >ALARMS</div>
				<div class="id_fasa'.$no.'">
					<div class="fasa_a_ols'.$no.'">0.00 A</div>
					<div class="fasa_b_ols'.$no.'">0.00 A</div>
					<div class="fasa_c_ols'.$no.'">0.00 A</div>
					<div class="fasa_n_ols'.$no.'">0.00 A</div>
				</div>
			</div>
			<div id="lcd_show_ols'.$no.'"></div>';
		}
		?>		
		<div id="arus">
			<div class="label">Ia: </div><div class="arus01 fasa_a_ols1"></div>
			<div class="label">Ib: </div><div class="arus01 fasa_b_ols1"></div>
			<div class="label">Ic: </div><div class="arus01 fasa_c_ols1"></div>
			<!--<div class="label">Ir: </div><div class="arus01 arus_sum"></div>-->
		</div>
		<div id="arus_ols2">
			<div class="label">Ia: </div><div class="arus01 fasa_a_ols2"></div>
			<div class="label">Ib: </div><div class="arus01 fasa_b_ols2"></div>
			<div class="label">Ic: </div><div class="arus01 fasa_c_ols2"></div>
			<!--<div class="label">Ir: </div><div class="arus01 arus_sum"></div>-->
		</div>
		<div id="arus_ols3">
			<div class="label">Ia: </div><div class="arus01 fasa_a_ols3"></div>
			<div class="label">Ib: </div><div class="arus01 fasa_b_ols3"></div>
			<div class="label">Ic: </div><div class="arus01 fasa_c_ols3"></div>
			<!--<div class="label">Ir: </div><div class="arus01 arus_sum"></div>-->
		</div>
		<div id="arus_ols4">
			<div class="label">Ia: </div><div class="arus01 fasa_a_ols4"></div>
			<div class="label">Ib: </div><div class="arus01 fasa_b_ols4"></div>
			<div class="label">Ic: </div><div class="arus01 fasa_c_ols4"></div>
			<!--<div class="label">Ir: </div><div class="arus01 arus_sum"></div>-->
		</div>
		<div id="arus_ols5">
			<div class="label">Ia: </div><div class="arus01 fasa_a_ols5"></div>
			<div class="label">Ib: </div><div class="arus01 fasa_b_ols5"></div>
			<div class="label">Ic: </div><div class="arus01 fasa_c_ols5"></div>
			<!--<div class="label">Ir: </div><div class="arus01 arus_sum"></div>-->
		</div>
		
		<div id="warna_target" class="">
			<div class="label">Ia: </div><div class="arus01 fasa_a_tar1"></div>
			<div class="label">Ib: </div><div class="arus01 fasa_b_tar1"></div>
			<div class="label">Ic: </div><div class="arus01 fasa_c_tar1"></div>
		</div>

		<div id="warna_target2" class="">
			<div class="label">Ia: </div><div class="arus01 fasa_a_tar2"></div>
			<div class="label">Ib: </div><div class="arus01 fasa_b_tar2"></div>
			<div class="label">Ic: </div><div class="arus01 fasa_c_tar2"></div>
		</div>
		<div id="warna_target3" class="">
			<div class="label">Ia: </div><div class="arus01 fasa_a_tar3"></div>
			<div class="label">Ib: </div><div class="arus01 fasa_b_tar3"></div>
			<div class="label">Ic: </div><div class="arus01 fasa_c_tar3"></div>
		</div>
		<div id="warna_target4" class="">
			<div class="label">Ia: </div><div class="arus01 fasa_a_tar4"></div>
			<div class="label">Ib: </div><div class="arus01 fasa_b_tar4"></div>
			<div class="label">Ic: </div><div class="arus01 fasa_c_tar4"></div>
		</div>
		<div id="arus_target" class="hide">
			<div class="label">I: </div><div class="arus01 arus_sum"></div>
			<div class="label">V: </div><div class="arus01 volt"></div>
			<div class="label">F: </div><div class="arus01 freq"></div>
		</div>
		<div id="label_arus_total" style="display:none;">Arus Total: <b class="arus_total">0</b></div>
		<div id="label_trafo">
			<div class="trafo1">Trafo 1</div>
			<div class="trafo2">Trafo 2</div>
			<div class="trafo3">Trafo 3</div>
			<div class="trafo4">Trafo 4</div>
		</div>
		<div>
			<div class="target2"></div>
			<div class="target3"></div>
			<div class="target4"></div>
		</div>
		
		<div class="" id="container">
			<img  style="width:1280px; height:auto; margin-left:10px;" src="images/das48p.jpg"/>
		</div>
		<div style="cursor:pointer; position:absolute; z-index:100; right:10px; top:20px; display:none;">
			<span class="zoomOut" style="font-size:80%; color:#fff;"><i class="fa fa-minus fa-2x"></i></span>&nbsp;
			<span class="zoomIn" style="font-size:120%; color:#fff;"><i class="fa fa-plus fa-1x"></i></span>&nbsp;
			<span class="zoomOff" style="font-size:120%; color:#fff;"><i class="fa fa-check-square fa-2x"></i></span>
		</div>
	</table>
	<style>
		#ols_det {
			border-radius: 15px;
			padding:20px;
			width:500px;
			height:auto;
			border:solid 1px #fff;
			background-color:#ededed;
			z-index:1000;
			/* position:absolute;
			top:20px;
			left:100px; */
		}

		#example2 {
			border: 2px solid #61ff30;
			border-radius: 50px 20px;
		}
		.cursor-pointer{
			cursor:pointer;
		}
	</style>
	<div id="form_det" style="display:none;">
		<?php
			$qry=$select->det_ols($kode);
			$row=mysql_fetch_array($qry);
		?>
		<div id="ols_det">
			<div style="float:right;"><i class="fa fa-times fa-2x cursor-pointer" onClick="$('#ols_det').hide()"></i></div>
			<table>
				<tr><td>Arus</td><td valign="top">:<b class="fasa_a" style="font-size:18px; width:60px; border:solid 0px #000;">0 A</b></td></tr>
				<tr><td>CT Primer</td><td>:<?php echo rp($row['ct_primer']);?> A</td><td>Tahapan</td><td>&nbsp;</td></tr>
				<tr><td>CT Sekunder</td><td>:<?php echo $row['ct_sekunder'];?> A</td><td>T1:</td><td><?php echo $row['t1'];?> S</td></tr>
				<tr><td>Ratio</td><td>:<?php echo $row['ratio'];?></td><td>T2:</td><td><?php echo $row['t2'];?> S</td></tr>
				<tr><td>Setting OLS</td><td>:<?php echo rp($row['set_ols']);?> A</td><td>T3:</td><td><?php echo $row['t3'];?> S</td></tr>
				<tr>
					<td>
					<button onClick="di_input('reset')" class="btn btn-warning cursor-pointer"><i class="fa fa-undo"></i> LED Reset</button>
					</td>
					<td>
					<button onClick="di_input('block')" class="btn btn-lg btn-danger cursor-pointer"><i class="fa fa-times-circle"></i> Blocking</button>
					</td>
					<td>
					<button onClick="di_input('unblock')" class="btn btn-lg btn-primary cursor-pointer"><i class="fa fa-lock"></i> UnBlocking</button>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<!--<tr><td>Kode</td>
					<td>:<input type="text" id="kode" value="" style="width:100px;"/></td>
				</tr>
				<tr><td>Password</td>
					<td>:<input type="password" id="pass" style="width:45px;"/>&nbsp;<input type="button" name="" value="Login" onClick="cek_login()"></td>
				</tr>-->
				<b id="btn-block"></b>
			</table>
		</div>
	</div>
	
	<!--<div class="col-md-12" style="display:inline;">
			<div class="btn-wrapper">
					<?php 
						$qry=$select->ols_list();
						while($row=mysql_fetch_array($qry)){
					?>
						<div id="id_<?php echo $row['kode'];?>_box" class="btn-trans btn-white" onClick="v_detail('<?php echo $row['kode'];?>','<?php echo $row['kode_gi'];?>')">
							<a href=""><?php echo $row['nama'];?></a>
							<div style="text-align:left; font-size:11px; border-top:solid 2px #fff;">
								<table id="id_<?php echo $row['kode'];?>" style="width:86px; border-radius:5px; margin-top:2px;">
								<a href="">
									<tr><td colspan="2" style="border:solid 0px #fff;">&nbsp;<b id="id_<?php echo $row['kode'];?>_s">STATUS</b></td></tr>
									<tr class=""><a href=""><td>&nbsp;Arus</td><td> : <b id="id_<?php echo $row['kode'];?>_i">0</b></td></a></tr>
									<tr class=""><a href=""><td>&nbsp;OLSID</td><td> :<b id="id_<?php echo $row['kode'];?>_uid"> <?php echo $row['kode'];?></b></td></a></tr>
									<tr class=""><a href=""><td>&nbsp;Conn</td><td> :<b id="id_<?php echo $row['kode'];?>_dc"></b></td></a></tr>
									<tr class="hide"><a href=""><td>&nbsp;Volt</td><td> :<b id="id_<?php echo $row['kode'];?>_v">0</b></td></a></tr>
									<tr class="hide"><a href=""><td>&nbsp;Notif</td><td> :<b id="sts_notif_<?php echo $row['kode'];?>">0</b></td></a></tr>
									<tr class="hide"><a href=""><td>&nbsp;UID1 :<b id="id_<?php echo $row['kode'];?>_nuid" style="font-size:10px;"> <?php echo $row['nama_unit'];?></b></td></a></tr>
									<tr class="hide"><a href=""><td>&nbsp;UID2 :<b id="id_<?php echo $row['kode'];?>_nuid2" style="font-size:10px;"> <?php echo $row['nama_unit'];?></b></td></a></tr>
									<tr class="hide"><a href=""><td>&nbsp;UnID :</td><td> :<b id="id_<?php echo $row['kode'];?>_unid"><?php echo $row['kode_unit'];?></b></td></a></tr>
								</a>
								</table>
							</div>
						</div>
					<?php
						}
					?>
				<div class="btn-trans" >
					<a href="#">
						<img class="plus" src="../../img/plus.png" onClick="loadMenu('004'); wcook('addols','t')">
					</a>
				</div>
			</div>
	</div>-->
	
</body>
</html>
<script src="js/jquery-1.7.1.min.js"></script>
<script src="js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="../../js/jquery.magnify.js"></script>
<script>
	function cek_login(){
		var kd=$('#kode').val();
		var ps=$('#pass').val();

		$.ajax({method: "GET",url:'../../mod/ols/cek_user.php?user='+kd+'&pas='+ps})
		.done(function( msg )
			{
				$('#btn-block').html(msg);
			}
		)
	}
	var jml_ols=$('#jml_ols').html();
		for(i=jml_ols;i>=2;i--){
		add_micom(i);
		$('#arus_ols'+i).css("display","block");
		$('#nama_ols'+i).css("display","block");
		$('#lcd_ols'+i).css("display","block");		
		
		//led control
		$('.trip_ols'+i).css("display","block");
		//$('.alarm_ols'+i).css("display","block");
		$('.warning_ols'+i).css("display","block");
		$('.healthy_ols'+i).css("display","block");
		$('.led5_ols'+i).css("display","block");
		$('.led6_ols'+i).css("display","block");
		$('.led7_ols'+i).css("display","block");
		$('.led8_ols'+i).css("display","block");
		}
		
		function add_blink_ols(i){
		var jml_ols=$('#jml_ols').html();
			for(i=jml_ols;i>=2;i--){
				for(x=0;x<=2;x++) {
				 $('.alarm_ols'+i).effect("pulsate", {}, 350);
				 $('.alarm_ols_det'+i).effect("pulsate", {}, 350);				 
				 }
			}
		}
		
		var nil_arusx=$('#niltot').html();
		var nil_arus=nil_arusx/4;
		
		$('.fasa_a_tar1').html(nil_arus +' A');
		//$('.fasa_b_tar1').html(nil_arus +' A');
		//$('.fasa_c_tar1').html(nil_arus +' A');
		
		$('.fasa_a_tar2').html(nil_arus +' A');
		//$('.fasa_b_tar2').html(nil_arus +' A');
		//$('.fasa_c_tar2').html(nil_arus +' A');
		
		$('.fasa_a_tar3').html(nil_arus +' A');
		//$('.fasa_b_tar3').html(nil_arus +' A');
		//$('.fasa_c_tar3').html(nil_arus +' A');
		
		$('.fasa_a_tar4').html(nil_arus +' A');
		//$('.fasa_b_tar4').html(nil_arus +' A');
		//$('.fasa_c_tar4').html(nil_arus +' A');
		
		 $(document).ready(function () {
        var currZoom = $("#container").css("zoom");
        //alert(currZoom)
        if (currZoom == 'normal') currZoom = 1; // For IE

        $(".zoomIn").click(function () {
            currZoom *= 1.2;
            $("#container").css("zoom", currZoom);
            $("#container").css("-moz-transform", "Scale(" + currZoom + ")");
            $("#container").css("-moz-transform-origin", "0 0");

        });

        $(".zoomOff").click(function () {
            $("#container").css("zoom", 1);
            $("#container").css("-moz-transform", "Scale(" + currZoom + ")");
            $("#container").css("-moz-transform-origin", "0 0");

        });
        $(".zoomOut").click(function () {
            currZoom *= .8;
            $("#container").css("zoom", currZoom);
            $("#container").css("-moz-transform", "Scale(" + currZoom + ")");
            $("#container").css("-moz-transform-origin", "0 0");

        });
    });
	
	$(document).keydown(function(event) {
	if (event.ctrlKey==true && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109'  || event.which == '187'  || event.which == '189'  ) ) {
		event.preventDefault();
	 }
	// 107 Num Key  +
	// 109 Num Key  -
	// 173 Min Key  hyphen/underscor Hey
	// 61 Plus key  +/= key
	});

	$(window).bind('mousewheel DOMMouseScroll', function (event) {
	   if (event.ctrlKey == true) {
	   event.preventDefault();
	   }
	});

</script>