<!doctype html>
<html lang="en">


<?php
 include 'session.php';
 require_once 'class/class.select.php';
 $select = new select;
 $userID = $_SESSION['userSession'];
 $area = $select->user($userID, 'area');

 if($area == 'PUSAT'){
     $area_login = '%';
 }else{
    $area_login = $area;
 }
?>

<head>
<title>Device Monitoring System</title>
<link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/vendor/toastr/toastr.min.css">
<link rel="stylesheet" href="assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<link rel="stylesheet" href="assets/vendor/sweetalert/sweetalert.css"/>

<!-- MAIN CSS -->
<link rel="stylesheet" href="theme/assets/css/main.css">
<link rel="stylesheet" href="theme/assets/css/color_skins.css">

<script src="assets/js/jquery.min.js"></script>
<script src="assets/datatables/js/jquery.dataTables.js"></script>
<script src="assets/js/init_notif.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#datatable').DataTable({
        ordering: false
    });
});
</script>

<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
</style>
</head>
<body class="theme-dark">


<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <div class="navbar-brand">
            </div>
            
           <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">    
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span id="notif-dot" class="notification-dot" style="display:none"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li class="header"><strong>You have <span id="status_notif"></span> new Notifications</strong></li>
                                <div id="notif-body" style="overflow-x: hidden; height: auto; max-height: 200px;"></div>
                                <li class="footer"><a href="notifikasi_list.php" class="more">See all notifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" onClick="logoutx()" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                 <img src="<?php echo 'assets/images/';
                if ($select->user($userID, 'photo') == '') {
                    echo 'f_avatar.png';
                } else {
                    echo $select->user($userID, 'photo');
                }; ?>" class="rounded-circle user-photo">

                <div class="dropdown">
                    <span>Welcome,</span>
                     <a href="" class="user-name"><strong> <?php echo $select->user($userID, 'role_role_id') . ' <br> ' .
                $select->user($userID, 'nama_depan'). ' '.$select->user($userID, 'nama_belakang') ; ?></strong></a>
                </div>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>                
            </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <?php
                           if ($select->user($userID, 'role_role_id') == 'SUPERADMIN') {                           
                        ?>  
                        <ul id="main-menu" class="metismenu">                          
                            <li>
                                <a href="home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="main/user_list.php"><i class="icon-users"></i> <span>User List</span></a>
                            </li>
                            <li>
                                <a href="main/list_wilayah2.php"><i class="icon-map"></i> <span>Region List</span></a>
                            </li>
                            <li>
                                <a href="main/mesin_list.php"><i class="icon-speedometer"></i> <span>Device List</span></a>
                            </li>
                            <li>
                                <a href="main/det_alat.php"><i class="icon-grid"></i> <span>Device Details</span></a>
                            </li>
							<li>
                                <a href="main/event_history.php"><i class="fa fa-bar-chart-o"></i> <span>Event History</span></a>
                            </li>
                            <li>
                                <a href="main/notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
							<li>
                                <a href="main/list_task.php"><i class="icon-briefcase"></i> <span>Task List</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                        <?php
                            }else{
                        ?>
                        <ul id="main-menu" class="metismenu">                          
                            <li>
                                <a href="home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="main/notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                        <?php
                            }
                        ?> 
                    </nav>
                </div>   
            </div>          
        </div>
    </div>
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-bars"></i></a> Notifikasi List</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item"><a href="../home.php"> Home</a></li>
                            <li class="breadcrumb-item active"><a href="notifikasi_list.php"> Notification List</a></li>
                        </ul>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        
                    </div>
                </div>
            </div>
            
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Notification List</h2>                       
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table width="100%" id="datatable" class="table" style="font-style:">
        
                                  <thead>
                                       <th></th>
                                       <th width="20%"><center></center></th>
                                 </thead>

                                <?php 
                                $no=1;
                                    $qry=$select->getListNotif($area_login);
                                    while($row=mysql_fetch_array($qry)){

                                    if ($row['status']=='N01'){
                                        $info = 'badge-success';
                                    }else if($row['status']=='N02'){
                                        $info = 'badge-warning';
                                    }else if($row['status']=='N03'){
                                        $info = 'badge-danger';
                                    }else{
                                        $info = 'badge-default';
                                    } 

                                    if($row['flag']== 'Y'){
                                        $flag = 'style="color: white"';
                                        $href = '#';
                                        $span = '';
                                    }else{
                                        $flag = '';
                                        $href = 'notifAction(\''.$row['status'].'\','.$row['id'].',\''.$row['kode'].'\')';
                                        $span = '<span class="badge badge-warning mb-0">new</span>';
                                    }
                                ?>
                                <tr style="background-color: transparent;">
                                    <td> <div class="mail-detail-right">
                                                <?php echo '<h6 class="sub"><a onclick="'.$href.'" href="javascript:void(0);" class="mail-detail-expand" '.$flag.'>'?> <?php print($row['nama']); ?></a> <?php echo '<span class="badge badge-default mb-0">'.$row['notif_name'].'</span>'.$span;?></h6>
                                                <p class="dep"><span class="m-r-10">
                                                <?php print(
                                         ($row['desa'] != null ? 'Desa '.$row['desa'].',' : '').' '
                                        .($row['kecamatan'] != null ? 'Kec. '.$row['kecamatan'].',' : '').' '
                                        .($row['kota'] != null ? $row['kota'].',' :'').' '
                                        .($row['provinsi'] != null ? 'Prov. '.$row['provinsi'] :'')); ?>
                                                </p>
                                                <span class="time"><?php print(date('d M Y h:i:sa', strtotime($row['tanggal']))); ?></span>
                                </div></td>
                                    <td><center>
                                    <button type="button"
                                    class="detail btn btn-info detail" onclick="detail_alat(<?php echo $row['id']?>)"><i class="fa fa-search"></i></button>
                                    </center></td>
                                </tr>
                                <?php
                                    }
                                ?>  
                             </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</div>

<!-- <div id="unimodal" name="unimodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title text text-danger unititle">Caption title</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p class="text text-muted unibody">Text body</p>
            </div>
            <div class="modal-footer">
                <button onclick="uact()" type="button" class="btn btn-success uniaction" data-dismiss="modal">Action
                    button
                </button>

                <button onclick="delfoto()" type="button" class="btn btn-danger uniaction-3"  data-dismiss="modal"><i>Delete Photo</i>
                </button>

                <button onclick="window.location ='user_list.php'" type="button" class="btn btn-default"
                        data-dismiss="modal">Close
                </button>
            </div>
        </div>
    </div>
</div> -->

<!-- <div class="modal fade" id="modalNotifikasi" name="modalNotifikasi" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bg-dark">
                <div class="modal-header">
                    <h6 class="panel-title"><b id="cap_add">Notification Detail</b> 
                        <b id="kode_gi"></b><b id="mx_kode_child"></b><b id="kode_group"></b></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                            onClick="dcook('addols','f')">&times;
                    </button>
                </div>
                <div class="modal-body bg-white">
                   <div class="row clearfix">
                       <div class="col-12">
                          <div class="panel-body content-body">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label class="label">Device Name</label>
                                    <div><input class="form-control" id="nama"  readonly="true"></div>
                                    </div>
                                    <div class="col-sm-4">
                                    <label class="label">Status</label>
                                    <div><input class="form-control" id="status123" readonly="true"></div>
                                    </div>
                                    <div class="col-sm-4">
                                    <label class="label">Date</label>
                                    <div><input class="form-control" id="tanggal" readonly="true"></div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                            <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">ia </span>
                                </div>
                                    <input type="text" id="ia" class="form-control" readonly="true">
                                </div>
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">ib </span>
                                </div>
                                    <input type="number" id="ib" class="form-control" readonly="false">
                                </div>
                                
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">ic </span>
                                </div>
                                    <input type="number" id="ic" class="form-control" readonly>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group row">
                            <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">va </span>
                                </div>
                                    <input type="number" id="va" class="form-control" readonly >
                                </div>
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">vb</span>
                                </div>
                                    <input type="number" id="vb" class="form-control" readonly>
                                </div>
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">vc </span>
                                </div>
                                    <input type="number" id="vc" class="form-control" readonly>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group row">
                            <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">ta </span>
                                </div>
                                    <input type="number" id="ta" class="form-control" readonly>
                                </div>
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">tb</span>
                                </div>
                                    <input type="number" id="tb" class="form-control" readonly>
                                </div>
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">tc </span>
                                </div>
                                    <input type="number" id="tc" class="form-control" readonly>
                                </div>
                            </div>
                            <br>
                          </div>
                       </div>
                  </div>
                  </div>
                  <input type="hidden" id="kode_mesin_updt">
                <div class="modal-footer">
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
    </div>
</div> -->

<div class="modal fade" id="modalAdd" name="modalAdd" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bg-dark">
            <!-- <div class="panel panel-default"> -->
                <div class="modal-header">
                    <h6 class="panel-title"><b id="cap_add">Caption title</b> 
                        <b id="kode_gi"></b><b id="mx_kode_child"></b><b id="kode_group"></b></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                            onClick="dcook('addols','f')">&times;
                    </button>
                </div>
                <div class="modal-body bg-white">
                   <div class="row clearfix">
                       <div class="col-12">
                          <div class="panel-body content-body">Content body</div>
                       </div>
                  </div>
                  </div>
                <div class="modal-footer">
				<button type="button" name="btnAdd" id="btnModify" class="btn btn-warning" onclick="modify()" style="display: none">Modify</button>
                <button type="button" name="btnAdd" id="btnApprove" class="btn btn-primary" onclick="approval()" style="display: none">Approve</button>
                       <button type="button" name="btnAdd" id="btnAdd" class="btn btn-primary" onClick="simpan()">Save</button>
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
    </div>
</div>
</div>

<audio id="status10">
  <source src="assets/sound/alarm.mp4" type="audio/mp4">
</audio>
<audio id="status11">
  <source src="assets/sound/train_low.mp3" type="audio/mp3">
</audio>

<!-- Javascript -->
<script src="sse/receive_sse.js"></script>
<script src="assets/js/hmi.js" type="text/javascript"></script>
<script src="assets/js/home.js" type="text/javascript"></script>
<script src="theme/assets/bundles/libscripts.bundle.js"></script>
<script src="assets/vendor/toastr/toastr.js"></script>    
<script src="theme/assets/bundles/vendorscripts.bundle.js"></script>
<script src="theme/assets/bundles/datatablescripts.bundle.js"></script>
<script src="assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
<script src="theme/assets/bundles/mainscripts.bundle.js"></script>
<script src="theme/assets/js/pages/tables/jquery-datatable.js"></script>

<script>

    function logoutx() {
        if (confirm("Are you sure to Log out?")) {
            window.location = "logout.php";
        }
    }

    function detail_alat(id){
    $('#btnModify, #btnApprove').hide();
	$('#btnAdd').hide();
    $('#modalAdd').modal('show'); 
    $('#cap_add').html('Detail Notifikasi');
    $('.content-body').load('process/detail_list.php?id='+id);
    }

    // $(function(){
    //     $('.detail').on('click', function(){ 
    //         $('#nama').val($(this).data("nama"));
    //         $('#status123').val($(this).data("sts"));
    //         $('#tanggal').val($(this).data("tanggal"));
    //         $('#ia').val($(this).data("ia"));
    //         $('#ib').val($(this).data("ib"));
    //         $('#ic').val($(this).data("ic"));
    //         $('#va').val($(this).data("va"));
    //         $('#vb').val($(this).data("vb"));
    //         $('#vc').val($(this).data("vc"));
    //         $('#ta').val($(this).data("ta"));
    //         $('#tb').val($(this).data("tb"));
    //         $('#tc').val($(this).data("tc"));
    //     });
    // });

</script>
</html>
