var datarealtime = [],
	totalPoints = 110;
	var updateInterval = 320;
	var realtime = 'on';

	$(function() {
		
	// $.plot($("#real_time_chart"), dataset, options);
	var plot = $.plot('#real_time_chart', [getRandomData()], {
		series: {
			shadowSize: 0,
			color: '#f51302'  //RED-R:#f51302  Yellow-S:#faf607   Blue-T:#0707fa
		},
		grid: {
			borderColor: '#27303e',
			borderWidth: 1,
			tickColor: '#27303e'
		},
		lines: {
			fill: true
		},
		yaxis: {
			min: 0,
			max: totalPoints-10
		},
		xaxis: {
			min: 0,
			max: totalPoints-10
		}
	});

	function updateRealTime() {
		plot.setData([getRandomData()]);
		plot.draw();

		var timeout;
		if (realtime === 'on') {
			timeout = setTimeout(updateRealTime, updateInterval);
		} else {
			clearTimeout(timeout);
		}
	}

	updateRealTime();

		$('#realtime').on('change', function() {
			realtime = this.checked ? 'on' : 'off';
			updateRealTime();
		});
	});

	function getRandomData() {
		if (datarealtime.length > 0) datarealtime = datarealtime.slice(1);

		while (datarealtime.length < totalPoints) {
			var prev = datarealtime.length > 0 ? datarealtime[datarealtime.length - 1] : 50,
				y = prev + Math.random() * 10 - 5;
			if (y < 0) {
				y = 0;
			} else if (y > 100) {
				y = 100;
			}

			datarealtime.push(y);
		}

		var res = [];
		for (var i = 0; i < datarealtime.length; ++i) {
			res.push([i, datarealtime[i]])
		}

		return res;
	}