<!doctype html>
<html lang="en">

<?php
session_start();
require_once 'module/login/class.user.php';
$user_home = new USER();

if (!$user_home->is_logged_in()) {
    $user_home->redirect('login.php');
}

$stmt = $user_home->runQuery("SELECT * FROM tbl_im_users WHERE userID=:uid");
$stmt->execute(array(":uid" => $_SESSION['userSession']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<?php
require 'class/class.select.php';
$select = new select;
$userID = $_SESSION['userSession'];
?>

<head>
<title>Device Monitoring System</title>
<link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<link rel="stylesheet" href="assets/vendor/sweetalert/sweetalert.css"/>

<!-- MAIN CSS -->
<link rel="stylesheet" href="theme/assets/css/main.css">
<link rel="stylesheet" href="css/custom.css">
<script src="js/hmi.js" type="text/javascript"></script>
<script src="js/home.js" type="text/javascript"></script>
<link rel="stylesheet" href="theme/assets/css/color_skins.css">
<link rel="stylesheet" type="text/css" href="assets/datatables/css/jquery.dataTables.css">
<script src="assets/js/jquery.min.js"></script>
<script src="assets/datatables/js/jquery.dataTables.js"></script>
<script src="assets/js/init_notif.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#datatable').DataTable();
});
</script>

<style>
    td.details-control {
    background: url('assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('assets/images/details_close.png') no-repeat center center;
    }
</style>
<script type="text/javascript" src="assets/js/loader.js"></script>
</head>
<body class="theme-dark">


<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <div class="navbar-brand">
            <audio id="trip_sound" src="module/ols/addons/train_low.mp3"></audio>
            <audio id="alarm_sound" src="module/ols/addons/alarm.mp4"></audio>
            <span class="hidden badge badge-pill badge-danger" style="display: none;"><b id="sts_notif1">status notif</b></span>
            <span class="hidden badge badge-pill badge-danger" style="display: none;"><b id="scanols_id">n.a</b></span>
            <span class="hidden badge badge-pill badge-danger" style="display: none;"><b id="sts_notif">0</b></span>
            <span class="hidden badge badge-pill badge-danger hide" style="display: none;"><b id="sts_notif0">0</b></span>
            <span class="hidden badge badge-pill badge-danger" style="display: none;"><b id="sts_notif2">f</b></span>
            <span class="hidden badge badge-pill badge-danger" style="display: none;"><b id="cid2">0</b></span>
            <span class="hidden badge badge-pill badge-danger" style="display: none;"><b id="cid">0</b></span>
            </div>

            <input type="checkbox" id="cls_ctrl" style="display: none;" />
            
            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span id="notif-dot" class="notification-dot" style="display:none"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li class="header"><strong>You have <span id="status_notif"></span> new Notifications</strong></li>
                                <div id="notif-body"></div>                            
                                <li class="footer"><a href="notifikasi_list.php" class="more">See all notifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" onClick="logoutx()" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                 <img src="<?php echo 'assets/images/';
                if ($select->user($userID, 'photo') == '') {
                    echo 'f_avatar.png';
                } else {
                    echo $select->user($userID, 'photo');
                }; ?>" class="rounded-circle user-photo">

                <div class="dropdown">
                    <span>Welcome,</span>
                     <a href="" class="user-name"><strong> <?php echo $select->user($userID, 'role_role_id') . ' <br> ' .
                $select->user($userID, 'nama_depan'). ' '.$select->user($userID, 'nama_belakang') ; ?></strong></a>
                </div>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>
            </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <ul id="main-menu" class="metismenu">                            
                            <li>
                                <a href="home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="main/user_list.php"><i class="icon-users"></i> <span>User List</span></a>
                            </li>
                            <li>
                                <a href="main/list_wilayah2.php"><i class="icon-map"></i> <span>Region List</span></a>
                            </li>
                            <li>
                                <a href="main/mesin_list.php"><i class="icon-speedometer"></i> <span>Device List</span></a>
                            </li>
                            <li class="active">
                                <a href="det_alat.php"><i class="icon-grid"></i> <span>Device Details</span></a>
                            </li>
                            <li>
                                <a href="main/notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>               
            </div>          
        </div>
    </div>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-bars"></i></a> Device Details</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item"><a href="home.php"> Home</a></li>
                            <li class="breadcrumb-item active"><a href="det_alat.php"> Device Details</a></li>
                        </ul>
                    </div>            
                </div>
            </div>
            
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Device Details</h2>                       
                        </div>
    <div class="container" id="container">
        <div id="main">
            <div class="row">
                <div class="col-md-12" style="display:inline;">
                    <div class="btn-wrapper">
                        <?php
                        $qrygi = $select->mesin_list_all();
                        while ($rowgi = mysql_fetch_array($qrygi)) {
                            ?>
                                <div id="id_<?php echo $rowgi['kode_mesin']; ?>_box" class="btn-trans btn-white" data-target="#detailalat" data-toggle="modal"   >
                                            <b><?php echo $rowgi['nama']; ?></b> 
                                            <i id="icon_<?php echo $rowgi['kode_mesin']; ?>" class="icon-volume fa" title="Mute/Unmute Alarm" onClick="">&nbsp;</i>
                                                <div title="Klik untuk info detail" onClick="cek_event('<?php echo $rowgi['event']; ?>')" style="text-align:left; cursor:pointer; font-size:11px; border-top:solid 2px #fff;">
                                                    <table align="center" id="id_<?php echo $rowgi['kode_mesin']; ?>"
                                                           style="width:80px; border-radius:5px; margin-top:2px;">                                                                                  
                                                            <td id="id_<?php echo $rowgi['kode_mesin']; ?>_dc" colspan="2" align="center" style="border:solid 0px red; " >
                                                                <img style="background-color:transparent; height:65px;" class="" src="assets/images/trf_grey.png">
                                                            </td>
                                                            <tr class="hiddenx">
                                                                <td colspan="2" style="border:solid 0px #fff;">
                                                                    <center>&nbsp;<b id="id_<?php echo $rowgi['kode_mesin']; ?>_s"></b></center>
                                                                </td>
                                                            </tr>
                                                            <tr class="hidden" style="display: none;">
                                                                        <td>&nbsp;I</td>
                                                                        <td> : <b id="id_<?php echo $rowgi['kode_mesin']; ?>_i">0</b></td>
                                                            </tr>
                                                            <tr class="hidden" style="display: none;">
                                                                        <td>&nbsp;V</td>
                                                                        <td> : <b id="id_<?php echo $rowgi['kode_mesin']; ?>_v">0</b></td>
                                                            </tr>
                                                    </table>
                                                </div>
                                </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <!--OLS List-->
            </div>
             <div id="main_sub" style="display:none;" onclick=""></div>
        </div>
    </div>
</div>
                    </div>
                </div>
				<?php include "modal_notif.php";?>
            </div>
        </div>
    </div>
</div>


<!-- Modal Popup untuk Detail--> 
<div id="detailalat" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content bg-dark">

      <div class="modal-header">
            <h6 class="modal-title" id="myModalLabel">TRAFO 2KV MEDAENG</h6>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>

        <div class="modal-body">
          <form action="datatable.php" name="modal_popup" enctype="multipart/form-data" method="POST">
              <div class="row">
              
              <div class="col-md-2">
                <center>
                  <img width="90px" height="90px" src="assets/images/b.png" data-target="#a" data-toggle="modal"/><br/><br/>
                  <label>Network Setting</label>
                </center>
              </div> 

              <div class="col-md-2">
                <center>
                <img width="90px" height="90px" src="assets/images/c.png" data-target="#devicesetting" data-toggle="modal"></a><br/><br/>
                <label>Device Setting</label>
                </center>
              </div>

              <!-- <div class="col-md-2" >
                <center>
                <a href="chanel_setting.php?id=001"><img width="90px" height="90px" src="../assets/images/d.png"></a><br/><br/>
                <label>Channel Setting</label>
                </center>
              </div> -->


                 <div class="col-md-2">
                <center>
                <img width="90px" height="90px" src="assets/images/time.png"  data-target="#bc" data-toggle="modal"/><br/><br/>
                  <label>Time Setting</label>
                </center> 
               </div>   
             

              <div class="col-md-2">
                <center>
                <a href="export.php?id=001"><img width="90px" height="90px" src="assets/images/e2.png"></a><br/><br/>
                <label>Download Log</label>
                </center>
              </div>

              <div class="col-md-2">
                <center>
                <a href="process/log_del.php?id=001" onclick="return confirm('Are you sure to delete data?');"><img width="90px" height="90px" src="assets/images/f.png" onclick=""></a><br/><br/>
                <label>Delete Log</label>
                </center>
              </div>
             </div>

              <div class="modal-footer">             
                  <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">
                    Close
                  </button>
              </div>
          </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal Popup untuk Network Setting--> 
 <?php
    include 'class/koneksi.php';
   $id = '001';
    $data = mysql_query("select * from network where kode_mesin= '$id'");
    while($d = mysql_fetch_array($data)){
?>

<div id="a" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content bg-dark">

      <div class="modal-header">
            <h6 class="modal-title" id="myModalLabel">Network Setting</h6>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>

        <div class="modal-body">
          <form action="process/network_upd.php" name="modal_popup" enctype="multipart/form-data" method="POST">

              <input type="hidden" name="id" value="<?php echo $d['kode_mesin'] ?>">

                <div class="row">
                <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Ip Server</label>
                 <input type="text" class="form-control" name="ipserver" value="<?php echo $d['ipserver'];?>" required/>
                </div>
               
                <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>IP Address</label>
                  <input class="form-control" type="text" id="" name="ipaddr" value="<?php echo $d['iplocal'];?>" required/>
               </div>
               </div>
                 
               <div class="row">
                 <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Netmask</label>
                   <input class="form-control" type="text" id="" name="netmask" value="<?php echo $d['netmask'];?>" required/>
                </div>


                 <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Gateway</label>
                  <input class="form-control" type="text" id="" name="gateway" value="<?php echo $d['gateway'];?>" required/>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12" style="padding-bottom: 20px;">
                  <label>DNS Server</label>
               <input class="form-control" type="text" id="" name="dns" value="<?php echo $d['dns'];?>" required/>
                </div>
              </div>

              <div class="row">
                <i><font color="white" style="padding-left: 15px;">*) Require reboot to affect new configuration</font></i>
              </div>

              <div class="modal-footer">
                  <button class="btn btn-success" type="submit" name="save">
                     Save
                  </button>
                   <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">
                    Close
                  </button>
              </div>
              </form>
            <?php
              }
              ?>
            </div>           
        </div>
    </div>
</div>



<!-- Modal Popup untuk Time Setting--> 

   <?php
   include 'class/koneksi.php';
   $id = '001';
   $data = mysql_query("select * from m_mesin where kode_mesin='$id'");
   while($d = mysql_fetch_array($data)){
   ?>

<div id="bc" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content bg-dark">

      <div class="modal-header">
            <h6 class="modal-title" id="myModalLabel">Time Setting</h6>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>

        <div class="modal-body">
          <form action="ins_time.php" name="modal_popup" enctype="multipart/form-data" method="POST">

            <input type="hidden" name="id" value="<?php echo $d['kode_mesin'] ?>">
                
                <div class="row">

                  <div class="col-md-4">
                  <center><label>Current</label></center>
                    <?php
                      $query2 = "SELECT * FROM m_mesin WHERE kode_mesin='$id'";
                      $result2 = mysql_query($query2);
                      while ($row = mysql_fetch_array($result2)){
                      $arus = $row['arus_value'];
                    ?> 
                    <select name="arus_capture" id="arus_capture" class="form-control" style="width: 150px; float:center;">
                      <option value="0">--Select--</option>
                      <option value="10" <?php if ($arus=="10") echo 'selected="selected"'; ?> >10 Minutes</option>
                      <option value="15" <?php if ($arus=="15") echo 'selected="selected"'; ?> >15 Minutes</option>
                      <option value="30" <?php if ($arus=="30") echo 'selected="selected"'; ?> >30 Minutes</option>
                      <?php } ?>
                    </select>                  
                  </div>
      
                  <div class="col-md-4">
                  <center><label>Voltage</label></center>
                   <?php
                      $query2 = "SELECT * FROM m_mesin WHERE kode_mesin='$id'";
                      $result2 = mysql_query($query2);
                      while ($row = mysql_fetch_array($result2)){
                      $tegangan = $row['volt_value'];
                   ?>
                   <select name="volt_capture" id="volt_capture" class="form-control" style="width: 150px; float:center;">
                      <option value="0">--Select--</option>
                      <option value="10" <?php if ($tegangan=="10") echo 'selected="selected"'; ?> >10 Minutes</option>
                      <option value="15" <?php if ($tegangan=="15") echo 'selected="selected"'; ?> >15 Minutes</option>
                      <option value="30" <?php if ($tegangan=="30") echo 'selected="selected"'; ?> >30 Minutes</option>
                      <?php } ?>
                   </select>
                  </div>
          
             
                  <div class="col-md-4">
                  <center><label>Temperature</label></center>
                   <?php
                      $query2 = "SELECT * FROM m_mesin WHERE kode_mesin='$id'";
                      $result2 = mysql_query($query2);
                      while ($row = mysql_fetch_array($result2)) {
                      $temperatur = $row['temp_value'];
                   ?>
                   <select name="suhu_capture" id="suhu_capture" class="form-control" style="width: 150px; float:center;">
                      <option value="0">--Select--</option>
                      <option value="10" <?php if ($temperatur=="10") echo 'selected="selected"'; ?> >10 Minutes</option>
                      <option value="15" <?php if ($temperatur=="15") echo 'selected="selected"'; ?> >15 Minutes</option>
                      <option value="30" <?php if ($temperatur=="30") echo 'selected="selected"'; ?> >30 Minutes</option>
                      <?php } ?>
                  </select>
                  </div>
               </div>
              <br/>

              <div class="row">
                <div class="col-md-4">
                  <center><label>All</label></center>
                   <?php
                      $query3 = "SELECT * FROM m_mesin WHERE kode_mesin='$id'";
                      $result3 = mysql_query($query3);
                      while ($row = mysql_fetch_array($result3)) {
                      $all = $row['all_value'];
                   ?>
                   <select name="all_capture" id="all_capture" class="form-control" style="width: 150px; float:center;">
                      <option value="0">--Select--</option>
                      <option value="10" <?php if ($all=="10") echo 'selected="selected"'; ?> >10 Minutes</option>
                      <option value="15" <?php if ($all=="15") echo 'selected="selected"'; ?> >15 Minutes</option>
                      <option value="30" <?php if ($all=="30") echo 'selected="selected"'; ?> >30 Minutes</option>
                      <?php } ?>
                  </select>
                  </div>
              </div>
              <br/><br/>

  
              <div class="modal-footer">
                <button class="btn btn-success" type="submit">
                     Save
                  </button>
                   <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">
                    Close
                  </button>
              </div>
              </form>
            <?php
              }
              ?>
            </div>      
        </div>
    </div>
</div>



<!-- Modal Popup untuk Device Setting--> 
 <?php
    include 'class/koneksi.php';
   $id = '001';
    $data = mysql_query("select * from m_mesin2 where kode_mesin='$id'");
    while($d = mysql_fetch_array($data)){
        ?>

<div id="devicesetting" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content bg-dark">

      <div class="modal-header">
            <h6 class="modal-title" id="myModalLabel">Device Setting</h6>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>

        <div class="modal-body">
          <form action="../process/device_upd.php" name="modal_popup" enctype="multipart/form-data" method="POST">

            <input type="hidden" name="id" value="<?php echo $d['kode_mesin'] ?>">
                
                <div class="row">
                <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Machine Code</label>
                 <input type="text" class="form-control" name="kode_mesin" value="<?php echo $d['kode_mesin'];?>" readonly/>
                </div>

                <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>GI Name</label>
               <input type="text" class="form-control" name="kode_gi" value="<?php echo $d['kode_gi'];?>" required/>
                </div>
                </div>

                <div class="row">
                 <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Device Name</label>
                  <input type="text" class="form-control" name="nama_gi" value="<?php echo $d['nama'];?>" required/>
                </div>


                 <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Bot Name</label>
                 <input type="text" class="form-control" name="botname" value="<?php echo $d['botname'];?>" required/>
                </div>
              </div>

              <div class="modal-footer">
                <button class="btn btn-success" type="submit">
                     Save
                  </button>
                   <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">
                    Close
                  </button>
              </div>

              </form>
            <?php
              }
              ?>
           </div>

           
        </div>
    </div>
</div>
<script>

    function logoutx() {
        if (confirm("Are you sure to Logout?")) {
            window.location = "logout.php";
        }
    }



cek_arus('<?php echo $row['kode_mesin'];?>');
$('#value').html('<?php echo $row['value'];?>');
</script>

<!-- Javascript -->
<script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="js/hmi.js" type="text/javascript"></script>
<script src="js/home.js" type="text/javascript"></script>


<script src="theme/assets/bundles/libscripts.bundle.js"></script>    
<script src="theme/assets/bundles/vendorscripts.bundle.js"></script>
<script src="theme/assets/bundles/datatablescripts.bundle.js"></script>
<script src="assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
<script src="theme/assets/bundles/mainscripts.bundle.js"></script>
<script src="theme/assets/js/pages/tables/jquery-datatable.js"></script>


<script>
    document.getElementById('cls_ctrl').focus();

    $(document).ready(function () {
        csys2();
        csys();
    });

</script>
<script type="text/javascript">
    function cek_event(x){
        if(x!='0'){
            v_detail2('<?php echo $rowgi['kode_mesin']; ?>','<?php echo $rowgi['kode_mesin']; ?>');
        }else{
            alert('Device state: \nPower Off');
        }
    }
</script>

</html>
