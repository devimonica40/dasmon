<!doctype html>
<html lang="en">


<?php
include '../session.php';
include '../class/class.select.php';
$select=new select;
$userID=$_SESSION['userSession'];
$max_kode=$select->zf(($select->max_kode()+1),4);


$role = $select->user($userID,'role_role_id');
$area = $select->user($userID, 'area');
if($area == 'PUSAT'){
    $area_login = '%';
}else{
    $area_login = $area;
}
?>

<head>
<title>Device Monitoring System</title>
<link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../assets/vendor/toastr/toastr.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/sweetalert/sweetalert.css"/>

<!-- MAIN CSS -->
<link rel="stylesheet" href="../theme/assets/css/main.css">
<link rel="stylesheet" href="../theme/assets/css/color_skins.css">
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/datatables/js/jquery.dataTables.js"></script>
<script src="../assets/js/init_notif_main.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#datatable').DataTable();
});
</script>

<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
</style>
</head>
<body class="theme-dark">


<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <div class="navbar-brand">
            </div>
            
           <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">    
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                 <span id="notif-dot" class="notification-dot" style="display:none"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li class="header"><strong>You have <span id="status_notif"></span> new Notifications</strong></li>
                                <div id="notif-body" style="overflow-x: hidden; height: auto; max-height: 200px;"></div>                           
                                <li class="footer"><a href="../notifikasi_list.php" class="more">See all notifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" onClick="logoutx()" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                 <img src="<?php echo '../assets/images/';
                if ($select->user($userID, 'photo') == '') {
                    echo 'f_avatar.png';
                } else {
                    echo $select->user($userID, 'photo');
                }; ?>" class="rounded-circle user-photo">

                 <div class="dropdown">
                    <span>Welcome,</span>
                   <a href="" class="user-name"><strong> <?php echo $select->user($userID, 'role_role_id') . ' <br> ' .
                $select->user($userID, 'nama_depan'). ' '.$select->user($userID, 'nama_belakang'). ' '.$select->user($userID, 'area') ; ?></strong></a>
                </div>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>                
            </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <ul id="main-menu" class="metismenu">                            
                            <li>
                                <a href="../home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li class="active">
                                <a href="user_list.php"><i class="icon-users"></i> <span>User List</span></a>
                            </li>
                            <li>
                                <a href="list_wilayah2.php"><i class="icon-map"></i> <span>Region List</span></a>
                            </li>
                            <li>
                                <a href="mesin_list.php"><i class="icon-speedometer"></i> <span>Device List</span></a>
                            </li>
                            <li>
                                <a href="det_alat.php"><i class="icon-grid"></i> <span>Device Details</span></a>
                            </li>
                           <li>
                                <a href="event_history.php"><i class="fa fa-bar-chart-o"></i> <span>Event History</span></a>
                            </li>
                            <li>
                                <a href="notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
							<li>
                                <a href="list_task.php"><i class="icon-briefcase"></i> <span>Task List</span></a>
                            </li>
                            <li>
                               <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>               
            </div>          
        </div>
    </div>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-bars"></i></a> User List</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item"><a href="../home.php"> Home</a></li>
                            <li class="breadcrumb-item active"><a href="user_list.php"> User List</a></li>
                        </ul>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        
                    </div>
                </div>
            </div>
            
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>User List</h2>                       
                        </div>
                        <div class="body">
                            <button type="button" onClick="$('#uid').html('<?php echo $max_kode; ?>');$('#uact').html('add'); pildata('<?php echo $max_kode;?>','add');" class="btn btn-primary m-b-15" data-toggle="modal" data-target="#unimodal"><i class="icon wb-plus" aria-hidden="true"></i>Add User</button>

                            <button type="button" class="btn btn-primary hidex" style="display: none"><b id="uid"></b>|<b id="uact"></b></button>
                            <div class="table-responsive">
                                <table width="100%" id="datatable" class="table" style="font-style:">
        
                                  <thead>
                                       <th><center>No.</center></th>
                                       <th><center>User ID</center></th>
                                       <th><center>Name</center></th>
                                       <th><center>Username</center></th>
                                       <th><center>Photo</center></th>
                                       <th><center>Role</center></th>
                                       <th><center>Active</center></th>
                                       <th width="20%"><center>Actions</center></th>
                                 </thead>

                                <?php 
                                $no=1;
                                    $qry=mysql_query("select * from tbl_im_users WHERE area LIKE '$area_login'");
                                    while($row=mysql_fetch_array($qry)){
                                        $json[$row['userID']] = json_encode($row);
                                ?>
        
                                <tr style="background-color: transparent;">
                                    <td><center><?php echo $no++;?></center></td>
                                    <td><center><?php print($row['userCode']); ?></center></td>
                                    <td><center><?php print($row['nama_depan']. ' ' . $row['nama_belakang']); ?></center></td>
                                    <td><center><?php print($row['userName']); ?></center></td>
                                    
                                     <td><center><img src="../assets/images/<?php print($row['photo']); 
                                     if ($row['photo']==""){ echo 'nophoto.jpg';}
                                     ?>" height='40px' width='40px'/></center></td>

                                    <td><center><?php print($row['role_role_id']); ?></center></td>
                                    <td class="text-center"><center><?php print($row['userStatus']); ?></center></td>

                                    
                                    <td><center>

                                    <button type="button" class="btn btn-primary" onclick="pildata('<?php echo $row['userID'];?>','upload')" data-user='<?= $json[$row['userID']]; ?>' data-toggle="modal" data-target="#unimodal">
                                      <i class="fa fa-upload"></i></button>  
       
                                       &nbsp;&nbsp;&nbsp;&nbsp;

                                    
                                      <button onclick="pildata('<?php echo $row['userID']; ?>','viewft')"  type="button" class="btn btn-primary"   data-toggle="modal" data-target="#unimodal" <?php if ($row['photo']==""){ echo 'disabled';} ?> ><i class="fa fa-picture-o" ></i></button>&nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                     <button type="button" class="btn btn-primary" onclick="pildata('<?php echo $row['userID'];?>','edit')" data-user='<?= $json[$row['userID']]; ?>' data-toggle="modal" data-target="#unimodal">
                                      <i class="fa fa-edit"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;

                                 
                                     <button type="button" class="btn btn-danger" onclick="pildata('<?php echo $row['userID'];?>','del')" data-userid="<?= $row['userID']; ?>" data-toggle="modal" data-target="#unimodal">
                                      <i class="fa fa-trash"></i></button></center>
                                   </td>
                                </tr>
                                <?php
                                    }
                                ?>  
                             </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
			<div id="unimodal" name="unimodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title text text-danger unititle">Caption title</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p class="text text-muted unibody">Text body</p>
            </div>
            <div class="modal-footer">
                <button onclick="uact()" type="button" class="btn btn-success uniaction" data-dismiss="modal">Action
                    button
                </button>

                <button onclick="delfoto()" type="button" class="btn btn-danger uniaction-3"  data-dismiss="modal"><i>Delete Photo</i>
                </button>

                <button onclick="window.location ='user_list.php'" type="button" class="btn btn-default"
                        data-dismiss="modal">Close
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAdd" name="modalAdd" tabindex="-1" role="dialog">
    <div class="modal-dialog" id="ukuran" role="document">
        <div class="modal-content bg-dark">
                <div class="modal-header">
                    <h6 class="panel-title"><b id="cap_add">Caption title</b> 
                        <b id="kode_gi"></b><b id="mx_kode_child"></b><b id="kode_group"></b></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                            onClick="dcook('addols','f')">&times;
                    </button>
                </div>
                <div class="modal-body bg-white">
                   <div class="row clearfix">
                       <div class="col-12">
                          <div class="panel-body content-body">Content body</div>
                       </div>
                  </div>
                  </div>
                <div class="modal-footer">
				 <button type="button" name="btnAdd" id="btnModify" class="btn btn-warning" onclick="modify()" style="display: none">Modify</button>
                <button type="button" name="btnAdd" id="btnApprove" class="btn btn-primary" onclick="approval()" style="display: none">Approve</button>
                       <button type="button" name="btnAdd" id="btnAdd" class="btn btn-primary" onClick="simpan()">Save</button>
                       <button id="btnClose" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
    </div>
</div>
</div>
			

        </div>
    </div>
    
</div>



<audio id="status10">
  <source src="../assets/sound/alarm.mp4" type="audio/mp4">
</audio>
<audio id="status11">
  <source src="../assets/sound/train_low.mp3" type="audio/mp3">
</audio>


<!-- Javascript -->
<script src="../assets/js/dialogs.js"></script>
<script src="../assets/js/hmi.js" type="text/javascript"></script>
<script src="../assets/js/home.js" type="text/javascript"></script>
<script src="../theme/assets/bundles/libscripts.bundle.js"></script>
<script src="../assets/vendor/toastr/toastr.js"></script>    
<script src="../theme/assets/bundles/vendorscripts.bundle.js"></script>
<script src="../theme/assets/bundles/datatablescripts.bundle.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="../assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
<script src="../theme/assets/bundles/mainscripts.bundle.js"></script>
<script src="../theme/assets/js/pages/tables/jquery-datatable.js"></script>
<script src="../module/sse/receive_sse.js"></script>
<script>

    function logoutx() {
        if (confirm("Are you sure to Log out?")) {
            window.location = "../logout.php";
        }
    }

function pildata(dt,x){
    var uact=$('#uact').html(x);
    if(x=='edit'){
        $('#uid').html(dt);
        $('.unititle').html('Change data');
        $('.unibody').html('Do you want to change data?');
        $('.uniaction').html('Change');
        $('.uniaction-3').hide();
        $('#btnAdd').show();

    }else if(x=='del'){
        $('#uid').html(dt);
        $('.unititle').html('Delete data');
        $('.unibody').html('Are you sure to delete data?');
        $('.uniaction').html('Delete');
        $('.uniaction-3').hide();
        $('#btnAdd').show();

    }else if(x=='view'){
        $('#uid').html(dt);
        $('.unititle').html('View data details');
        $('.unibody').html('Do you want to see data details?');
        $('.uniaction').html('Detail');
        $('.uniaction-3').hide();
        $('#btnAdd').show();

        }else if(x=='viewft'){
        $('#uid').html(dt);
        $('.unititle').html('View photo details');
        $('.unibody').html('Do you want to see photo details?');
        $('.uniaction').html('Detail');
        $('.uniaction-3').html();
        $('#btnAdd').show();

    }else if(x=='add'){
        $('#uid').html(dt);
        $('.unititle').html('Add data');
        $('.unibody').html('Do you want to add data?');
        $('.uniaction').html('Add');
        $('.uniaction-3').hide();
        $('#btnAdd').show();

    }else if(x=='upload'){
        $('#uid').html(dt);
        $('.unititle').html('Add Photo');
        $('.unibody').html('Do you want to add photo?');
        $('.uniaction').html('Add');
        $('.uniaction-3').hide();
        $('#btnAdd').hide();
        $('#btnClose').hide();
    }
} 
function unimodal(uid,act){
    $('#unimodal').modal('show'); 
    $('#uid').html(uid);
    $('.unititle').html('Pesan..');
    $('.unibody').html('Process '+act+ ' succes!');
    $('.uniaction').html('OK'); 
}
function uact(){
    var uid=$('#uid').html();
    var uact=$('#uact').html();
    if(uact=='edit'){
        edit(uid);
    }else if(uact=='del'){
        del(uid);
    }else if(uact=='view'){
        view(uid);
    }else if(uact=='viewft'){
        viewft(uid);    
    }else if(uact=='add'){
        add(uid);
    }else if(uact=='upload'){
        upload(uid);
    }
}

function del(uid){  
    $.ajax({method: "GET",url:'../process/user_del.php?kode='+uid})
     .done(function( ms )
     {  
       $('#modalAdd').modal('hide');
           toastr.info(
              'Data successfully deleted!',
              '',
              {
                timeOut: 1000,
                fadeOut: 1000,
                positionClass: 'toast-top-center',
                onHidden: function () {
                    window.location.reload();
                  }
              }
            );   
     }
    )
}

function add(){
	$('#ukuran').removeClass('modal-lg').addClass('modal-md');
    $('#btnModify, #btnApprove').hide();
	$('#btnAdd').show();
    $('#modalAdd').modal('show');
    $('#cap_add').html('Add User');
    $('.content-body').load('../process/user_add.php');
}

function upload(uid){
    $('#modalAdd').modal('show');
    $('#cap_add').html('Add Photo');
    $('.content-body').load('../process/user_upload.php?kode='+uid); 
}

function edit(uid){
	$('#ukuran').removeClass('modal-lg').addClass('modal-md');
    $('#btnModify, #btnApprove').hide();
	$('#btnAdd').show();
    $('#modalAdd').modal('show'); 
    $('#cap_add').html('Change data');
    $('.content-body').load('../process/user_add.php?kode='+uid); 
}

function viewft(uid){
    $('#modalAdd').modal('show'); 
    $('#cap_add').html('View photo');
    $('.content-body').load('../process/user_viewft.php?kode='+uid); 
}

function simpan(){
    var uact=$('#uact').html();
    if(uact=='edit'){
        upd_user();
    }else if(uact=='add'){
        ins_user();
    }
}


function ins_user(){ 
 if(
    $('#txtname1').val() != '' && $('#txtname2').val() != '' && 
    $('#txtemail').val() != '' && $('#role').val() != '' &&
    $('#psw1').val() != '' && $('#area').val() != '' ){

   $.post( "../process/user_ins.php", { 
        txtname1:$('input[name=txtname1]').val(),
        txtname2:$('input[name=txtname2]').val(),
        txtemail:$('input[name=txtemail]').val(),
        cmbRole:$('#role').val(),
        txtpass:$('input[name=txtpass]').val(),
        area:$('input[name=area]').val()
    })
     .done(function(data) {
        $('#modalAdd').modal('hide');
           toastr.info(
              'Data successfully inserted!',
              '',
              {
                timeOut: 1000,
                fadeOut: 1000,
                positionClass: 'toast-top-center',
                onHidden: function () {
                    window.location.reload();
                  }
              }
            );
     });
 }else{
        alert('Data cannot be empty');
 }                
}

function upd_user(){
        $.post( "../process/user_upd.php", { 
            txtname1:$('input[name=txtname1]').val(),
            txtname2:$('input[name=txtname2]').val(),
            txtemail:$('input[name=txtemail]').val(),
            cmbRole:$('#role').val(),
            userStatus:$('.userStatusId').attr('checked'),
            txtpass:$('input[name=txtpass]').val(),
            userID:$('input[name=userID]').val(),
            upd_psw:$('#upd_psw').html(),
            area:$('input[name=area]').val()
        }) 
        .done(function(data) {
           $('#modalAdd').modal('hide');
           toastr.info(
              'Data successfully updated!',
              '',
              {
                timeOut: 1000,
                fadeOut: 1000,
                positionClass: 'toast-top-center',
                onHidden: function () {
                    window.location.reload();
                  }
              }
            );
        });
}

function delfoto(){
    confirm("Are you sure to delete photo?");    
    var uid=$('#uid').html();
    $.ajax({method: "GET",url: '../process/user_delfoto.php?userCode='+uid})
    .done(function(ms)
    {
        $('#modalAdd').modal('hide');
           toastr.info(
              'Photo successfully delete!',
              '',
              {
                timeOut: 1000,
                fadeOut: 1000,
                positionClass: 'toast-top-center',
                onHidden: function () {
                    window.location.reload();
                  }
              }
            );
    }
    );
}
</script>
</html>
