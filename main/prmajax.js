function suggest_desa(src)
{
	var page    = 'suggest_desa.php';
	if(src.length>2){
		var loading = '<p align="center">Loading ...</p>';
		showStuff('suggest_desa');
		$('#suggest_desa').html(loading);
		$.ajax({
			url: page,
			data : 'nama_desa='+src,
			type: "post", 
			dataType: "html",
			timeout: 10000,
			success: function(response){
				$('#suggest_desa').html(response);
			}
		});
	}
}

//Fungsi untuk memilih provinsi dan memasukkannya pada input text
function pilih_desa(desa)
{
	$('#nama_desa').val(desa);
}


function suggest_kec(src)
{
	var page    = 'suggest_kec.php';
	if(src.length>=2){
		var loading = '<p align="center">Loading ...</p>';
		showStuff('suggest_kec');
		$('#suggest_kec').html(loading);
		$.ajax({
			url: page,
			data : 'nama_kecamatan='+src,
			type: "post", 
			dataType: "html",
			timeout: 10000,
			success: function(response){
				$('#suggest_kec').html(response);
			}
		});
	} 
}

//Fungsi untuk memilih provinsi dan memasukkannya pada input text
function pilih_kecamatan(kecamatan)
{
	$('#nama_kecamatan').val(kecamatan);
}



function suggest(src)
{
	var page    = 'suggest.php';
	if(src.length>=3){
		var loading = '<p align="center">Loading ...</p>';
		showStuff('suggest');
		$('#suggest').html(loading);
		$.ajax({
			url: page,
			data : 'nama_kota='+src,
			type: "post", 
			dataType: "html",
			timeout: 10000,
			success: function(response){
				$('#suggest').html(response);
			}
		});
	}
}

//Fungsi untuk memilih kota dan memasukkannya pada input text
function pilih_kota(kota)
{
	$('#nama_kota').val(kota);
}

function suggest_prov(src)
{
	var page    = 'suggest_prov.php';
	if(src.length>=2){
		var loading = '<p align="center">Loading ...</p>';
		showStuff('suggest_prov');
		$('#suggest_prov').html(loading);
		$.ajax({
			url: page,
			data : 'nama_provinsi='+src,
			type: "post", 
			dataType: "html",
			timeout: 10000,
			success: function(response){
				$('#suggest_prov').html(response);
			}
		});
	}
}

//Fungsi untuk memilih provinsi dan memasukkannya pada input text
function pilih_provinsi(provinsi)
{
	$('#nama_provinsi').val(provinsi);
}

//menampilkan form div
function showStuff(id) {
	document.getElementById(id).style.display = 'block';
}
//menyembunyikan form
function hideStuff(id) {
	document.getElementById(id).style.display = 'none';
}