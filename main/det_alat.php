﻿<!doctype html>
<html lang="en">

<?php
include '../session.php';
include '../class/class.select.php';
$select=new select;
$userID=$_SESSION['userSession'];
$max_kode=$select->zf(($select->max_kode()+1),4);

$role = $select->user($userID,'role_role_id');
$area = $select->user($userID, 'area');
//echo $area;
$max_kode=$select->zf(($select->max_kode()+1),4);
if($area == 'PUSAT'){
    $area_login = '%';
}else{
    $area_login = $area;
} 
?>

<head>
<title>Device Monitoring System</title>
<link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="../assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/sweetalert/sweetalert.css"/>

<!-- MAIN CSS -->
<link rel="stylesheet" href="../theme/assets/css/main.css">
<link rel="stylesheet" href="../assets/css/custom2.css">
<link rel="stylesheet" href="../theme/assets/css/color_skins.css">

<link rel="stylesheet" type="text/css" href="../assets/datatables/css/jquery.dataTables.css">
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/datatables/js/jquery.dataTables.js"></script>
<script src="../assets/js/init_notif_main.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	var kode = localStorage.getItem('healty');
    var nama = localStorage.getItem('nama_healty');

    if (kode != null){
        document.getElementById("new_healty").style.display = "inline-block";
        setTimeout(function(){
            document.getElementById("new_healty").style.display = "none";
            localStorage.removeItem('healty');
            localStorage.removeItem('nama_healty');
        },9000)
    }else{
        document.getElementById("new_healty").style.display = "none";
    }
    $('#nama_trafo').html(nama);
	
    $('#datatable').DataTable();
});
</script>

<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
	}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
	@-webkit-keyframes blinker {
		from {opacity: 1.0;}
		to {opacity: 0.0;}
	}
	.blink{
		text-decoration: blink;
		-webkit-animation-name: blinker;
		-webkit-animation-duration: 0.6s;
		-webkit-animation-iteration-count:infinite;
		-webkit-animation-timing-function:ease-in-out;
		-webkit-animation-direction: alternate;
	}
</style>
<script type="text/javascript" src="../assets/js/loader.js"></script>
</head>
<body class="theme-dark">


<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <div class="navbar-brand">
            </div>
            
            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                       
                       
                       
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                  <span id="notif-dot" class="notification-dot" style="display:none"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li class="header"><strong>You have <span id="status_notif"></span> new Notifications</strong></li>
                                <div id="notif-body" style="overflow-x: hidden; height: auto; max-height: 200px;"></div>
                                <li class="footer"><a href="../notifikasi_list.php" class="more">See all notifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" onClick="logoutx()" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                 <img src="<?php echo '../assets/images/';
                if ($select->user($userID, 'photo') == '') {
                    echo 'f_avatar.png';
                } else {
                    echo $select->user($userID, 'photo');
                }; ?>" class="rounded-circle user-photo">

                <div class="dropdown">
                    <span>Welcome,</span>
                     <a href="" class="user-name"><strong> <?php echo ucwords($role) . ' <br> ' .
                $select->user($userID, 'nama_depan'). ' '.$select->user($userID, 'nama_belakang'). ' '.$select->user($userID, 'area') ; ?></strong></a>
                </div>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>
            </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <ul id="main-menu" class="metismenu">                            
                            <li>
                                <a href="../home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="user_list.php"><i class="icon-users"></i> <span>User List</span></a>
                            </li>
                            <li>
                                <a href="list_wilayah2.php"><i class="icon-map"></i> <span>Region List</span></a>
                            </li>
                            <li>
                                <a href="mesin_list.php"><i class="icon-speedometer"></i> <span>Device List</span></a>
                            </li>
                            <li class="active">
                                <a href="det_alat.php"><i class="icon-grid"></i> <span>Device Details</span></a>
                            </li>
                            <li>
                                <a href="event_history.php"><i class="fa fa-bar-chart-o"></i> <span>Event History</span></a>
                            </li>
                            <li>
                                <a href="notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="list_task.php"><i class="icon-briefcase"></i> <span>Task List</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>               
            </div>          
        </div>
    </div>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-bars"></i></a> Device Details</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item"><a href="../home.php"> Home</a></li>
                            <li class="breadcrumb-item active"><a href="det_alat.php"> Device Details</a></li>
                        </ul>
                    </div>            
                </div>
            </div>
            
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Device Details</h2>                       
                        </div>
                        <div class="row">
                        <div class="col-md-2">
                       <!--  <button style="margin-left: 15px;" id="addToTable" class="btn btn-primary m-b-15" type="button">
                                <i class="icon wb-plus" aria-hidden="true"></i> Add Device
                        </button> -->
                        </div>

                        <form action="det_alat.php" method="POST">
                        <div class="row">
                        <div class="col-md-2">
                                <input type="text" name="search" id="search" onkeyup="searchTable()" placeholder="Search" class="form-control" style="width: 200px; color: white; margin-left: 640px;">
                                <input style="display: none;" type="submit" value="Cari">

                        </div>
                        </div> <!-- tutup row-->
                        </form>


                       
                        <div class="body">

                          
                          <div id="responsecontainer">
                            <table style="display: block;" width="100%" id="tbl_alat">
                              <tbody>
                              
                              </tbody>
                            </table>
                          </div>
                          
                        <!--   <hr style="border: 1px solid grey; margin-left: 5px; margin-right: 5px;"> -->
                          

                          <table style="display: block;" width="100%" id="tbl_normal">
                           
                          <?php
                          $halaman = 27;
                          $page = isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;
                          $mulai = ($page>1) ? ($page * $halaman) - $halaman : 0; 
                          $data = $select->get_trafo_by_area($area_login);
                          $total = mysql_num_rows($data);
                          $pages = ceil($total/$halaman);
                          $query = mysql_query("SELECT
                                      ia.kode AS kodeia, 
                                      ia.tanggal, 
                                      ia.arus AS arusia,
                                      ib.kode AS kodeib, 
                                      ib.arus AS arusib,
                                      ic.kode AS kodeic, 
                                      ic.arus AS arusic,
                                      va.kode AS kodeva, 
                                      va.volt AS vola,
                                      vb.kode AS kodevb, 
                                      vb.volt AS volb,
                                      vc.kode AS kodevc, 
                                      vc.volt AS volc,
                                      ta.kode AS kodeta, 
                                      ta.temp AS tempa,
                                      tb.kode AS kodetb, 
                                      tb.temp AS tempb,
                                      tc.kode AS kodetc, 
                                      tc.temp AS tempc,
                                      freqa.kode AS kodefa, 
                                      freqa.freq AS freqa,
                                      freqb.kode AS kodefb, 
                                      freqb.freq AS freqb,
                                      freqc.kode AS kodefc, 
                                      freqc.freq AS freqc,
                                      msn.kode_mesin, 
                                      msn.latitude, 
                                      msn.longitude, 
                                      msn.nama, 
                                      msn.ratio, 
                                      msn.dc,
                                      msn.provinsi_name,
                                      msn.kota_name,
                                      msn.kecamatan_name,
                                      msn.desa_name,
                                      event_log.event as evt, 
                                      event_log.status,
                                      MAX(ia.tanggal) tgl_ia,
                                      MAX(ib.tanggal) tgl_ib,
                                      MAX(ic.tanggal) tgl_ic,
                                      MAX(va.tanggal) tgl_va,
                                      MAX(vb.tanggal) tgl_vb,
                                      MAX(vc.tanggal) tgl_vc,
                                      MAX(ta.tanggal) tgl_ta,
                                      MAX(tb.tanggal) tgl_tb,
                                      MAX(tc.tanggal) tgl_tc,
                                      MAX(freqa.tanggal) tgl_freqa,
                                      MAX(freqb.tanggal) tgl_freqb,
                                      MAX(freqc.tanggal) tgl_freqc,
                                      MAX(event_log.tanggal) tgl_even_log
                                      FROM m_mesin msn
                                        INNER JOIN event_log ON msn.kode_mesin = event_log.kode
                                      LEFT JOIN ia ON msn.kode_mesin = ia.kode 
                                      LEFT JOIN ib ON msn.kode_mesin = ib.kode
                                      LEFT JOIN ic ON ic.kode = msn.kode_mesin
                                      LEFT JOIN va ON va.kode = msn.kode_mesin
                                      LEFT JOIN vb ON vb.kode = msn.kode_mesin
                                      LEFT JOIN vc ON vc.kode = msn.kode_mesin
                                      LEFT JOIN ta ON ta.kode = msn.kode_mesin
                                      LEFT JOIN tb ON tb.kode = msn.kode_mesin
                                      LEFT JOIN tc ON tc.kode = msn.kode_mesin
                                      LEFT JOIN freqa ON freqa.kode = msn.kode_mesin
                                      LEFT JOIN freqb ON freqb.kode = msn.kode_mesin
                                      LEFT JOIN freqc ON freqc.kode = msn.kode_mesin
                                      WHERE msn.provinsi_name LIKE '$area'
                                      GROUP BY msn.kode_mesin  LIMIT $mulai, $halaman")or die(mysql_error);
									  
							$no = 0;
							
                          while ($rowgi = mysql_fetch_assoc($query)) {
							$no++;
                            if($no == 1){
                                    echo '<tr style="display: none;" id="new_healty"> 
                                    <td style="display: inline-block;">
                                    <div id="id_0098_box" class="btn-trans btn-green" data-target="#detailalat" data-toggle="modal">
                                                <b id="nama_trafo"></b> 
                                                 <i id="icon_0098" class="icon-volume fa" title="Mute/Unmute Alarm" onClick="">&nbsp;</i>
                                                    <div title="Klik untuk info detail" style="text-align:left; cursor:pointer; font-size:11px; border-top:solid 2px #fff;">
                                                        <table align="center" id="id_0098"
                                                               style="width:80px; border-radius:5px; margin-top:2px;"> 
                                                                <tr>                                               
                                                                <td id="id_0098_dc" colspan="2" align="center" style="border:solid 0px red;" >
                                                                    <img style="background-color:transparent; height:65px;" class="blink" src="../assets/images/trf_green.png">
                                                                </td>
                                                              </tr>
                                                                <tr>
                                                                    <td colspan="2" style="border:solid 0px #fff;">
                                                                        <center>&nbsp;<b id="id_0098_s">HEALTY</b></center>
                                                                    </td>
                                                                </tr>
                                                                <p id="statustrafo"></p>
                                                                <tr class="hidden" style="display: none;">
                                                                            <td>&nbsp;I</td>
                                                                            <td> : <b id="id_0098_i">0</b></td>
                                                                </tr>
                                                                <tr class="hidden" style="display: none;">
                                                                            <td>&nbsp;V</td>
                                                                            <td> : <b id="id_0098_v">0</b></td>
                                                                </tr> 
    
                                                        </table>
                                                        
                                                 </div>
                                    </div>
                                    </td>
                                   </tr>';
                            }
                          ?>
                          
                          <tr style="display: inline-block;" id="id_<?php echo $rowgi['kode_mesin']; ?>_tr"> 
                                <td style="display: inline-block;">
                                <div id="id_<?php echo $rowgi['kode_mesin']; ?>_box" class="btn-trans btn-white"  >
                                            <b id="nama_<?php echo $rowgi['kode_mesin']; ?>"><?php echo $rowgi['nama']; ?></b> 
                                             <i id="icon_<?php echo $rowgi['kode_mesin']; ?>" class="icon-volume fa" title="Mute/Unmute Alarm" onClick="">&nbsp;</i>
                                                <div onClick="cek_event('<?php echo $rowgi['event']; ?>')" style="text-align:left; cursor:pointer; font-size:11px; border-top:solid 2px #fff;">
                                                    <table align="center" id="id_<?php echo $rowgi['kode_mesin']; ?>"
                                                           style="width:80px; border-radius:5px; margin-top:2px;"> 
                                                            <tr>                                               
                                                            <td id="id_<?php echo $rowgi['kode_mesin']; ?>_dc" colspan="2" align="center" style="border:solid 0px red;" >
                                                                <img style="background-color:transparent; height:65px;" class="center" src="../assets/images/trf_green.png" id="gambar_<?php echo $rowgi['kode_mesin'];?>">
                                                            </td>
                                                          </tr>
                                                            <tr>
                                                                <td colspan="2" style="border:solid 0px #fff;">
                                                                    <center>&nbsp;<b id="id_<?php echo $rowgi['kode_mesin']; ?>_s"></b></center>
                                                                </td>
                                                            </tr>
                                                            <p id="statustrafo"></p>
                                                            <tr class="hidden" style="display: none;">
                                                                        <td>&nbsp;I</td>
                                                                        <td> : <b id="id_<?php echo $rowgi['kode_mesin']; ?>_i">0</b></td>
                                                            </tr>
                                                            <tr class="hidden" style="display: none;">
                                                                        <td>&nbsp;V</td>
                                                                        <td> : <b id="id_<?php echo $rowgi['kode_mesin']; ?>_v">0</b></td>
                                                            </tr> 

                                                    </table>
                                             </div>
                                </div>
                                </td>
                               </tr>
                            
                            <?php
                        }
                        ?>
                        </table>
                        <div class="mt-2 ml-2">
                          <?php for ($i=1; $i<=$pages ; $i++){ ?>
                          <a href="?halaman=<?php echo $i; ?>"><button style="margin-left: 0px;" class="btn btn-outline-light"><?php echo $i; ?></button></a>

                          <?php } ?>

                        </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</div>


<!-- Modal Popup untuk Detail--> 
<div id="detailalat" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content bg-dark">

      <div class="modal-header">
            <h6 class="modal-title" id="myModalLabel">??</h6>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>

        <div class="modal-body">
          <form action="datatable.php" name="modal_popup" enctype="multipart/form-data" method="POST">
              <div class="row">
              
              <div class="col-md-2">
                <center>
                  <img width="90px" height="90px" src="../assets/images/b.png" data-target="#a" data-toggle="modal"/><br/><br/>
                  <label>Network Setting</label>
                </center>
              </div> 

              <div class="col-md-2">
                <center>
                <img width="90px" height="90px" src="../assets/images/c.png" data-target="#devicesetting" data-toggle="modal"></a><br/><br/>
                <label>Device Setting</label>
                </center>
              </div>

              <!-- <div class="col-md-2" >
                <center>
                <a href="chanel_setting.php?id=001"><img width="90px" height="90px" src="../assets/images/d.png"></a><br/><br/>
                <label>Channel Setting</label>
                </center>
              </div> -->


                 <div class="col-md-2">
                <center>
                <img width="90px" height="90px" src="../assets/images/time.png"  data-target="#bc" data-toggle="modal"/><br/><br/>
                  <label>Time Setting</label>
                </center> 
               </div>   
             

              <div class="col-md-2">
                <center>
                <a href="export.php?id=001"><img width="90px" height="90px" src="../assets/images/e2.png"></a><br/><br/>
                <label>Download Log</label>
                </center>
              </div>

              <div class="col-md-2">
                <center>
                <a href="../process/log_del.php?id=001" onclick="return confirm('Are you sure to delete data?');"><img width="90px" height="90px" src="../assets/images/f.png" onclick=""></a><br/><br/>
                <label>Delete Log</label>
                </center>
              </div>
             </div>

              <div class="modal-footer">             
                  <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">
                    Close
                  </button>
              </div>
          </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal Popup untuk Network Setting--> 
 <?php
   // include '../class/koneksi.php';
    $id = '0001';
    $data = mysql_query("select * from network where kode_mesin= '$id'");
    while($d = mysql_fetch_array($data)){
?>

<div id="a" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content bg-dark">

      <div class="modal-header">
            <h6 class="modal-title" id="myModalLabel">Network Setting</h6>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>

        <div class="modal-body">
          <form action="../process/network_upd.php" name="modal_popup" enctype="multipart/form-data" method="POST">

              <input type="hidden" name="id" value="<?php echo $d['kode_mesin'] ?>">

                <div class="row">
                <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Ip Server</label>
                 <input type="text" class="form-control" name="ipserver" value="<?php echo $d['ipserver'];?>" required/>
                </div>
               
                <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>IP Address</label>
                  <input class="form-control" type="text" id="" name="ipaddr" value="<?php echo $d['iplocal'];?>" required/>
               </div>
               </div>
                 
               <div class="row">
                 <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Netmask</label>
                   <input class="form-control" type="text" id="" name="netmask" value="<?php echo $d['netmask'];?>" required/>
                </div>


                 <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Gateway</label>
                  <input class="form-control" type="text" id="" name="gateway" value="<?php echo $d['gateway'];?>" required/>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12" style="padding-bottom: 20px;">
                  <label>DNS Server</label>
               <input class="form-control" type="text" id="" name="dns" value="<?php echo $d['dns'];?>" required/>
                </div>
              </div>

              <div class="row">
                <i><font color="white" style="padding-left: 15px;">*) Require reboot to affect new configuration</font></i>
              </div>

              <div class="modal-footer">
                  <button class="btn btn-success" type="submit" name="save">
                     Save
                  </button>
                   <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">
                    Close
                  </button>
              </div>
              </form>
            <?php
              }
              ?>
            </div>           
        </div>
    </div>
</div>



<!-- Modal Popup untuk Time Setting--> 

   <?php
   //include '../class/koneksi.php';
   $id = '0001';
   $data = mysql_query("select * from m_mesin where kode_mesin='$id'");
   while($d = mysql_fetch_array($data)){
   ?>

<div id="bc" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content bg-dark">

      <div class="modal-header">
            <h6 class="modal-title" id="myModalLabel">Time Setting</h6>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>

        <div class="modal-body">
          <form action="ins_time.php" name="modal_popup" enctype="multipart/form-data" method="POST">

            <input type="hidden" name="id" value="<?php echo $d['kode_mesin'] ?>">
                
                <div class="row">

                  <div class="col-md-4">
                  <center><label>Current</label></center>
                    <?php
                      $query2 = "SELECT * FROM m_mesin WHERE kode_mesin='$id'";
                      $result2 = mysql_query($query2);
                      while ($row = mysql_fetch_array($result2)){
                      $arus = $row['arus_value'];
                    ?> 
                    <select name="arus_capture" id="arus_capture" class="form-control" style="width: 150px; float:center;">
                      <option value="0">--Select--</option>
                      <option value="10" <?php if ($arus=="10") echo 'selected="selected"'; ?> >10 Minutes</option>
                      <option value="15" <?php if ($arus=="15") echo 'selected="selected"'; ?> >15 Minutes</option>
                      <option value="30" <?php if ($arus=="30") echo 'selected="selected"'; ?> >30 Minutes</option>
                      <?php } ?>
                    </select>                  
                  </div>
      
                  <div class="col-md-4">
                  <center><label>Voltage</label></center>
                   <?php
                      $query2 = "SELECT * FROM m_mesin WHERE kode_mesin='$id'";
                      $result2 = mysql_query($query2);
                      while ($row = mysql_fetch_array($result2)){
                      $tegangan = $row['volt_value'];
                   ?>
                   <select name="volt_capture" id="volt_capture" class="form-control" style="width: 150px; float:center;">
                      <option value="0">--Select--</option>
                      <option value="10" <?php if ($tegangan=="10") echo 'selected="selected"'; ?> >10 Minutes</option>
                      <option value="15" <?php if ($tegangan=="15") echo 'selected="selected"'; ?> >15 Minutes</option>
                      <option value="30" <?php if ($tegangan=="30") echo 'selected="selected"'; ?> >30 Minutes</option>
                      <?php } ?>
                   </select>
                  </div>
          
             
                  <div class="col-md-4">
                  <center><label>Temperature</label></center>
                   <?php
                      $query2 = "SELECT * FROM m_mesin WHERE kode_mesin='$id'";
                      $result2 = mysql_query($query2);
                      while ($row = mysql_fetch_array($result2)) {
                      $temperatur = $row['temp_value'];
                   ?>
                   <select name="suhu_capture" id="suhu_capture" class="form-control" style="width: 150px; float:center;">
                      <option value="0">--Select--</option>
                      <option value="10" <?php if ($temperatur=="10") echo 'selected="selected"'; ?> >10 Minutes</option>
                      <option value="15" <?php if ($temperatur=="15") echo 'selected="selected"'; ?> >15 Minutes</option>
                      <option value="30" <?php if ($temperatur=="30") echo 'selected="selected"'; ?> >30 Minutes</option>
                      <?php } ?>
                  </select>
                  </div>
               </div>
              <br/>

              <div class="row">
                <div class="col-md-4">
                  <center><label>All</label></center>
                   <?php
                      $query3 = "SELECT * FROM m_mesin WHERE kode_mesin='$id'";
                      $result3 = mysql_query($query3);
                      while ($row = mysql_fetch_array($result3)) {
                      $all = $row['all_value'];
                   ?>
                   <select name="all_capture" id="all_capture" class="form-control" style="width: 150px; float:center;">
                      <option value="0">--Select--</option>
                      <option value="10" <?php if ($all=="10") echo 'selected="selected"'; ?> >10 Minutes</option>
                      <option value="15" <?php if ($all=="15") echo 'selected="selected"'; ?> >15 Minutes</option>
                      <option value="30" <?php if ($all=="30") echo 'selected="selected"'; ?> >30 Minutes</option>
                      <?php } ?>
                  </select>
                  </div>
              </div>
              <br/><br/>

  
              <div class="modal-footer">
                <button class="btn btn-success" type="submit">
                     Save
                  </button>
                   <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">
                    Close
                  </button>
              </div>
              </form>
            <?php
              }
              ?>
            </div>      
        </div>
    </div>
</div>




<script>
    cek_arus('<?php echo $row['kode_mesin'];?>');
    $('#value').html('<?php echo $row['value'];?>');
</script>


<!-- Modal Popup untuk Device Setting--> 
 <?php
   // include '../class/koneksi.php';
   $id = '0001';
    $data = mysql_query("select * from m_mesin2 where kode_mesin='$id'");
    while($d = mysql_fetch_array($data)){
        ?>

<div id="devicesetting" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content bg-dark">

      <div class="modal-header">
            <h6 class="modal-title" id="myModalLabel">Device Setting</h6>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>

        <div class="modal-body">
          <form action="../process/device_upd.php" name="modal_popup" enctype="multipart/form-data" method="POST">

            <input type="hidden" name="id" value="<?php echo $d['kode_mesin'] ?>">
                
                <div class="row">
                <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Machine Code</label>
                 <input type="text" class="form-control" name="kode_mesin" value="<?php echo $d['kode_mesin'];?>" readonly/>
                </div>

                <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>GI Name</label>
               <input type="text" class="form-control" name="kode_gi" value="<?php echo $d['kode_gi'];?>" required/>
                </div>
                </div>

                <div class="row">
                 <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Device Name</label>
                  <input type="text" class="form-control" name="nama_gi" value="<?php echo $d['nama'];?>" required/>
                </div>


                 <div class="col-md-6" style="padding-bottom: 20px;">
                  <label>Bot Name</label>
                 <input type="text" class="form-control" name="botname" value="<?php echo $d['botname'];?>" required/>
                </div>
              </div>

              <div class="modal-footer">
                <button class="btn btn-success" type="submit">
                     Save
                  </button>
                   <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">
                    Close
                  </button>
              </div>

              </form>
            <?php
              }
              ?>
           </div>

           
        </div>
    </div>
</div>

<div class="modal fade" id="modalAdd" name="modalAdd" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bg-dark">
            <!-- <div class="panel panel-default"> -->
                <div class="modal-header">
                    <h6 class="panel-title"><b id="cap_add">Caption title</b> 
                        <b id="kode_gi"></b><b id="mx_kode_child"></b><b id="kode_group"></b></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                            onClick="dcook('addols','f')">&times;
                    </button>
                </div>
                <div class="modal-body bg-white">
                   <div class="row clearfix">
                       <div class="col-12">
                          <div class="panel-body content-body">Content body</div>
                       </div>
                  </div>
                  </div>
                <div class="modal-footer">
                <button type="button" name="btnAdd" id="btnModify" class="btn btn-warning" onclick="modify()" style="display: none">Modify</button>
                <button type="button" name="btnAdd" id="btnApprove" class="btn btn-primary" onclick="approval()" style="display: none">Approve</button>
                       <button type="button" name="btnAdd" id="btnAdd" class="btn btn-primary" onClick="simpan()">Save</button>
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
    </div>
</div>
</div>
<script type="text/javascript">
    function searchTable() {
    var input;
    var saring;
    var status; 
    var tbody; 
    var tr; 
    var td;
    var i; 
    var j;
    input = document.getElementById("search");
    saring = input.value.toUpperCase();
    tbody = document.getElementsByTagName("tbody")[0];;
    tr = document.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td");
        for (j = 0; j < td.length; j++) {
            if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1) {
                status = true;
            }
        }
        if (status) {
            tr[i].style.display = "inline-block";
            status = false;
        } else {
            tr[i].style.display = "none";
        }
    }
}


    function logoutx() {
        if (confirm("Are you sure to Logout?")) {
            window.location = "../logout.php";
        }
    }
</script>

<audio id="status10">
  <source src="../assets/sound/alarm.mp4" type="audio/mp4">
</audio>
<audio id="status11">
  <source src="../assets/sound/train_low.mp3" type="audio/mp3">
</audio>

<!-- Javascript -->
<script src="../module/sse/receive_sse.js"></script>
<script src="../theme/assets/bundles/libscripts.bundle.js"></script>    
<script src="../theme/assets/bundles/vendorscripts.bundle.js"></script>
<script src="../theme/assets/bundles/datatablescripts.bundle.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="../assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
<script src="../theme/assets/bundles/mainscripts.bundle.js"></script>
<script src="../theme/assets/js/pages/tables/jquery-datatable.js"></script>

</html>
 