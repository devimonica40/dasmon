<!doctype html>
<html lang="en">

<?php
include '../session.php';
include '../class/class.select.php';
$select=new select;
$userID=$_SESSION['userSession'];
$max_kode=$select->zf(($select->max_kode()+1),4);

if(isset($_COOKIE['addols'])){
    $add_ols=$_COOKIE['addols'];
}else{ 
    $add_ols='f';
}
?>

<head>
<title>Device Monitoring System</title>
<link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../assets/vendor/toastr/toastr.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/sweetalert/sweetalert.css"/>

<!-- MAIN CSS -->
<link rel="stylesheet" href="../theme/assets/css/main.css">
<link rel="stylesheet" href="../theme/assets/css/color_skins.css">

<link rel="stylesheet" type="text/css" href="../assets/datatables/css/jquery.dataTables.css">
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable').DataTable();
});
</script>

<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
</style>
<style>
input{display: block;margin-bottom: .25em;}
#suggest{z-index:5; padding-left: 27px;}
span.pilihan{display:block;cursor:pointer;}
#suggest_prov{ padding-left: 27px;}
#suggest_kec{ padding-left: 27px;}
#suggest_desa{ padding-left: 27px;}
</style>

<script src="prmajax.js"></script>
</head>
<body class="theme-dark">


<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <div class="navbar-brand">
            </div>
            
           <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                       
                       
                       
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span class="notification-dot"></span>                              
                                <span class="nodot"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li class="header"><strong>You have <span id="status"></span> new Notifications</strong></li>
                                <div id="notif-body"></div>                            
                                <li class="footer"><a href="#" class="more">See all notifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" onClick="logoutx()" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                 <img src="<?php echo '../assets/images/';
                if ($select->user($userID, 'photo') == '') {
                    echo 'f_avatar.png';
                } else {
                    echo $select->user($userID, 'photo');
                }; ?>" class="rounded-circle user-photo">

                <div class="dropdown">
                    <span>Welcome,</span>
                     <a href="" class="user-name"><strong> <?php echo $select->user($userID, 'role_role_id') . ' <br> ' .
                $select->user($userID, 'nama_depan'). ' '.$select->user($userID, 'nama_belakang') ; ?></strong></a>
                </div>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>                
            </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <ul id="main-menu" class="metismenu">                            
                            <li>
                                <a href="../home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="user_list.php"><i class="icon-users"></i> <span>User List</span></a>
                            </li>
                            <li>
                                <a href="list_wilayah2.php"><i class="icon-map"></i> <span>Region List</span></a>
                            </li>
                            <li class="active">
                                <a href="mesin_list.php"><i class="icon-speedometer"></i> <span>Device List</span></a>
                            </li>
                            <li>
                                <a href="det_alat.php"><i class="icon-grid"></i> <span>Device Details</span></a>
                            </li>
                            <li>
                                <a href="event_history.php"><i class="fa fa-bar-chart-o"></i> <span>Event History</span></a>
                            </li>
                            <li>
                                <a href="notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>            
            </div>          
        </div>
    </div>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-bars"></i></a> Device List</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item"><a href="../home.php"> Home</a></li>
                            <li class="breadcrumb-item active"><a href="mesin_list.php"> Device List</a></li>
                        </ul>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                       
                    </div>
                </div>
            </div>
            
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Device List</h2>                       
                        </div>
                        <div class="body">
                            <button type="button" onClick="$('#uid').html('<?php echo $max_kode; ?>');$('#uact').html('add'); pildata('<?php echo $max_kode;?>','add');" class="btn btn-primary m-b-15" data-toggle="modal" data-target="#unimodal"><i class="icon wb-plus" aria-hidden="true"></i>Add Device</button>
                            <button type="button" class="btn btn-primary hidex" style="display: none"><b id="uid"></b>|<b id="uact"></b></button>
                            <div class="table-responsive">
                                <table width="100%" id="datatable" class="table" style="font-style:">
        
                                  <thead>
                                      <th><center>No.</center></th>
                                      <th><center>MQTT</center></th>
                                      <th><center>Device Name</center></th>
                                      <th><center>CT Primer</center></th>
                                      <th><center>CT Secondary</center></th>
                                      <th><center>Active</center></th>
                                      <th width="20%"><center>Actions</center></th>
                                 </thead>

                                <?php 
                                  $no = 1;
                                  $qry=$select->mesin_list_all();
                                  while($row=mysql_fetch_array($qry)){
                                  $json[$row['id']] = json_encode($row);
                                ?>


        
                                <tr style="background-color: transparent;">
                                     <td><center><?php echo $no++; ?></center></td>
                                     <td><?php echo $row['desa_name']. ", " .
                                                       $row['kecamatan_name']. ", " . 
                                                       $row['kota_name']. ", " . 
                                                       $row['provinsi_name'] ; ?>
                                                        
                                     </td>
                                     <td><?php echo $row['trafo_name']; ?></td>
                                     <td><?php echo $row['ct_primer']; ?></td>
                                     <td><?php echo $row['ct_sekunder']; ?></td>
                                     <td><?php echo $row['active']; ?></td>
                                     
                                      <td><center>
                                       <button type="button" onclick="pildata('<?php echo $row['kode_mesin'];?>','addmqtt')" class="btn btn-warning" data-toggle="modal" data-target="#unimodal"> <i class="icon-speedometer"></i></button>
                                       &nbsp;&nbsp;

                                      <button type="button" onclick="pildata('<?php echo $row['kode_mesin'];?>','edit')"  class="btn btn-info" data-toggle="modal" data-target="#unimodal"><i class="fa fa-edit"></i></button>  &nbsp;&nbsp;

                                      <button type="button" onclick="pildata('<?php echo $row['kode_mesin'];?>','del')" class="btn btn-danger" data-toggle="modal" data-target="#unimodal"> <i class="fa fa-trash"></i></button>
                                      </center> 
                                      </td>


                                </tr>
                                <?php
                                    }
                                ?>  
                             </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> 
    </div>
    
</div>



<div id="unimodal" name="unimodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title text text-danger unititle">Caption title</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p class="text text-muted unibody">Text body</p>
            </div>
            <div class="modal-footer">
                <button onclick="uact()" type="button" class="btn btn-success uniaction" data-dismiss="modal">Action
                    button
                </button>

                <button onclick="window.location ='mesin_list.php'" type="button" class="btn btn-default"
                        data-dismiss="modal">Close
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAdd" name="modalAdd" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bg-dark">
            <!-- <div class="panel panel-default"> -->
                <div class="modal-header">
                    <h6 class="panel-title"><b id="cap_add">Caption title</b> 
                        <b id="kode_gi"></b><b id="mx_kode_child"></b><b id="kode_group"></b></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                            onClick="dcook('addols','f')">&times;
                    </button>
                </div>
                <div class="modal-body bg-white">
                   <div class="row clearfix">
                       <div class="col-12">
                          <div class="panel-body content-body">Content body</div>
                       </div>
                  </div>
                  </div>
                <div class="modal-footer">
                       <button type="button" name="btnAdd" id="btnAdd" class="btn btn-primary" onClick="simpan()">Save</button>
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
    </div>
</div>


<audio id="status10">
  <source src="../assets/sound/alarm.mp4" type="audio/mp4">
</audio>
<audio id="status11">
  <source src="../assets/sound/train_low.mp3" type="audio/mp3">
</audio>

<!-- Javascript -->
<script src="../module/sse/receive_sse.js"></script>
<script src="../theme/assets/bundles/libscripts.bundle.js"></script>    
<script src="../theme/assets/bundles/vendorscripts.bundle.js"></script>
<script src="../assets/vendor/toastr/toastr.js"></script>    
<script src="../theme/assets/bundles/datatablescripts.bundle.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="../assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
<script src="../theme/assets/bundles/mainscripts.bundle.js"></script>
<script src="../theme/assets/js/pages/tables/jquery-datatable.js"></script>

<script>
function pildata(dt,x){
    var uact=$('#uact').html(x);
    if(x=='edit'){
        $('#uid').html(dt);
        $('.unititle').html('Change data');
        $('.unibody').html('Do you want to change data?');
        $('.uniaction').html('Change');
        $('.uniaction-3').hide();

    }else if(x=='del'){
        $('#uid').html(dt);
        $('.unititle').html('Delete data');
        $('.unibody').html('Do you want to delete data?');
        $('.uniaction').html('Delete');
        $('.uniaction-3').hide();

    }else if(x=='add'){
        $('#uid').html(dt);
        $('.unititle').html('Add data');
        $('.unibody').html('Do you want to add data?');
        $('.uniaction').html('Add');
        $('.uniaction-3').hide();

    }else if(x=='addmqtt'){
        $('#uid').html(dt);
        $('.unititle').html('Add data');
        $('.unibody').html('Do you want to add mqtt data?');
        $('.uniaction').html('Add');
        $('.uniaction-3').hide();

    }
} 
function unimodal(uid,act){
    $('#unimodal').modal('show'); 
    $('#uid').html(uid);
    $('.unititle').html('Pesan..');
    $('.unibody').html('Process '+act+ ' succes!');
    $('.uniaction').html('');
}
function uact(){
    var uid=$('#uid').html();
    var uact=$('#uact').html();
    if(uact=='edit'){
        edit(uid);
    }else if(uact=='del'){
        del(uid);
    }else if(uact=='add'){
        add(uid);
    }else if(uact=='addmqtt'){
        addmqtt(uid);
    }
}

function del(uid){  
    $.ajax({method: "GET",url:'../process/mesin_del.php?kode='+uid})
     .done(function(data) {
       $('#modalAdd').modal('hide');
           toastr.info(
              'Data successfully deleted!',
              '',
              {
                timeOut: 1000,
                fadeOut: 1000,
                positionClass: 'toast-top-center',
                onHidden: function () {
                    window.location.reload();
                  }
              }
            );   
     });    
}

function add(uid){
    $('#modalAdd').modal('show');
    $('#cap_add').html('Add Region');
    $('.content-body').load('../process/mesin_add.php');
}

function edit(uid){
    $('#modalAdd').modal('show'); 
    $('#cap_add').html('Change data');
    $('.content-body').load('../process/mesin_add.php?kode='+uid);
   
}

function addmqtt(uid){
    $('#modalAdd').modal('show'); 
    $('#cap_add').html('Change data');
    $('.content-body').load('../process/mesin_add.php?kode='+uid);
}


function simpan(){
    var uact=$('#uact').html();
    if(uact=='edit'){
        upd_mesin();
    }else if(uact=='add'){
        ins_mesin();
    }else if(uact=='addmqtt'){
        ins_mqtt();
    }
}

function upd_mesin(){
    if($('#uid').html()!=''){
        $.post( "../process/mesin_upd.php", { 
        max_kode: '<?php echo $max_kode;?>',
        kode_mesin: $('#kode_mesin').val(), 
        nama_provinsi:$('#nama_provinsi').val(),
        nama_kota:$('#nama_kota').val(),
        nama_kecamatan:$('#nama_kecamatan').val(),
        nama_desa:$('#nama_desa').val(),
        namamesin:$('#nama_mesin').val(),
        jenismesin:$('#jenis_mesin').val(),
        ip:$('#ip').val(),      
        kapasitas:$('#kapasitas').val(),
        ct_primer:$('#ct_primer').val(),
        ct_sekunder: $('#ct_sekunder').val(),
        ratio: $('#ratio').val(),
        set_ols: $('#set_ols').val(),
        max_volt: $('#max_volt').val(),
        max_temp: $('#max_temp').val(),
        latitude: $('#latitude').val(),
        longitude: $('#longitude').val(),
        active:$("input[name=active]:checked").val(),
        keterangan:$('#keterangan').val()
        })
       .done(function(data) {
             $('#modalAdd').modal('hide');
           toastr.info(
              'Data successfully updated!',
              '',
              {
                timeOut: 1000,
                fadeOut: 1000,
                positionClass: 'toast-top-center',
                onHidden: function () {
                    window.location.reload();
                  }
              }
            );
     });    
    }else{ alert('Select data to be updated.!')}
}


function ins_mqtt(){
        // var kd_widget = ls_read('addwidget');
        $.post (
            "../process/mqtt_add.php", 
            { 
            panel_name:$('#panel_name').val(),
            payload_min:$("#payload_min").val(),
            payload_max:$("#payload_max").val(),
             panel_topic: $('#panel_topic').val(),
            unit:$("#unit").val(),
            qos:$("#qos").val(),
            description: $('#description').val()
            }
            )
        
        .done(function(data) {
            //alert("aku");
            $('#modalAdd').modal('hide');
           toastr.info(
              'Data successfully inserted!',
              '',
              {
                timeOut: 1000,
                fadeOut: 1000,
                positionClass: 'toast-top-center',
               
              }
            );
     });    
}





function ins_mesin(){
    if($('#nama_mesin').val().length > 0){
        $.post (
            "../process/mesin_ins.php", 
            { 
            max_kode: '<?php echo $max_kode;?>', 
            nama_provinsi:$('#nama_provinsi').val(),
            nama_kota:$('#nama_kota').val(),
            nama_kecamatan:$('#nama_kecamatan').val(),
            nama_desa:$('#nama_desa').val(),
            namamesin:$('#nama_mesin').val(),
            jenismesin:$('#jenis_mesin').val(),
            ip:$('#ip').val(),
            active:$("input[name=active]:checked").val(),
            kapasitas:$('#kapasitas').val(),
            ct_primer:$('#ct_primer').val(),
            ct_sekunder: $('#ct_sekunder').val(),
            ratio: $('#ratio').val(),
            set_ols: $('#set_ols').val(),
            max_volt: $('#max_volt').val(),
            max_temp: $('#max_temp').val(),
            latitude: $('#latitude').val(),
            longitude: $('#longitude').val(),
            keterangan:$('#keterangan').val()
            }
            )

        .done(function(data) {
            $('#modalAdd').modal('hide');
           toastr.info(
              'Data successfully inserted!',
              '',
              {
                timeOut: 1000,
                fadeOut: 1000,
                positionClass: 'toast-top-center',
                onHidden: function () {
                    window.location.reload();
                  }
              }
            );
     });    
    }else{
        alert('Data cannot be empty');
    }
}

function logoutx() {
        if (confirm("Are you sure to Logout?")) {
            window.location = "../logout.php";
        }
    }

</script>
</html>
